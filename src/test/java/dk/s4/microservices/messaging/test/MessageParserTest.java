package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.MessageIncompatibleException;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessageParser;
import java.io.File;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MessageParserTest {

    private static final String MESSAGE_STRING_FILE = "Message.json";
    private static final ClassLoader classLoader = MessageParserTest.class.getClassLoader();

    @Test
    public void parse() throws ParseException, MessageParseException, IOException, MessageIncompatibleException {
        String messageAsString = readFile(MESSAGE_STRING_FILE, StandardCharsets.UTF_8);
        Message m = MessageParser.parse(messageAsString);

        Assert.assertTrue(!m.getSender().isEmpty());
        Assert.assertTrue(m.getBodyCategory() == Message.BodyCategory.FHIR);
        Assert.assertTrue(!m.getBodyType().isEmpty());
        Assert.assertTrue(!m.getContentVersion().isEmpty());
        Assert.assertTrue(m.getPrefer() == Message.Prefer.REPRESENTATION);
        Assert.assertTrue(!m.getEtag().isEmpty());
        Assert.assertTrue(!m.getIfMatch().isEmpty());
        Assert.assertTrue(!m.getIfNoneExist().isEmpty());
        Assert.assertTrue(!m.getLocation().isEmpty());
        Assert.assertTrue(!m.getCorrelationId().isEmpty());
        Assert.assertTrue(!m.getTransactionId().isEmpty());
        Assert.assertTrue(!m.getSecurity().isEmpty());
        Assert.assertTrue(!m.getSession().isEmpty());

        Assert.assertTrue(!m.getBody().isEmpty());
    }

    private static String readFile(String path, Charset encoding) throws IOException {
        return new String(Files.readAllBytes(resourcePath(path)), encoding);
    }

    private static Path resourcePath(String path) {
        return Paths.get(classLoader.getResource(path).getPath());
    }
}
