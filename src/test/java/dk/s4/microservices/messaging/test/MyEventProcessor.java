package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.EventOperationOutcome;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyEventProcessor implements EventProcessor {
    private final Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);

    @Override
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage) {
        logger.debug("processMessage");

        messageProcessedTopic
                .setOperation(Topic.Operation.DataCreated)
                .setDataCategory(consumedTopic.getDataCategory())
                .setDataType(consumedTopic.getDataType())
                .setDataCodes(consumedTopic.getDataCodes());
        outgoingMessage
                .setBody(new EventOperationOutcome(EventOperationOutcome.INFORMATION,"success").toString())
                .setBodyCategory(Message.BodyCategory.FHIR)
                .setBodyType("OperationOutcome")
                .setContentVersion(System.getenv("FHIR_VERSION"))
                .setCorrelationId(receivedMessage.getCorrelationId())
                .setTransactionId(receivedMessage.getTransactionId());

        return true;
    }
}