package dk.s4.microservices.messaging.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import com.google.common.io.Resources;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessageParser;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.messaging.mock.MockKafkaConsumeAndProcess;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class MockKafkaMessagingTest {

    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(MockKafkaMessagingTest.class);

    private static int ourPort;

    private static MockProducer<String, String> mockOurKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;

    private static String ENVIRONMENT_FILE = "kafka.env";
    private static String KAFKA_PRODUCER_PROPS = "KafkaProducer.props";

    private static String OBSERVATION_MDC_29112 = "ObservationMDC29112.json";
    private static String INPUT_TOPIC_MDC_29112 = "InputReceived_FHIR_Observation_MDC29112";
    private static String OUTPUT_TOPIC_MDC_29112 = "DataCreated_FHIR_Observation_MDC29112";

    private static final String MESSAGE_STRING_FILE = "Message.json";
    private static final ClassLoader classLoader = MockKafkaMessagingTest.class.getClassLoader();

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private static String readResourceFile(String path, Charset encoding) throws IOException {
        return new String(Files.readAllBytes(resourcePath(path)), encoding);
    }

    private static Path resourcePath(String path) {
        return new File(classLoader.getResource(path).getFile()).toPath();
    }

    @Test
    public void testEventConsumerInterceptor()
            throws KafkaInitializationException, MessagingInitializationException, IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        MockKafkaConsumeAndProcess mockKafkaConsumeAndProcess = null;
        Thread mockKafkaConsumeAndProcessThread = null;

        try {
            List<Topic> topics = new ArrayList<>();
            Topic topic = new Topic()
                    .setOperation(Topic.Operation.InputReceived)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation")
                    .addDataCode("MDC29112");
            topics.add(topic);
            mockKafkaEventProducer = new MockKafkaEventProducer("MockKafkaMessagingTest");
            EventProcessor eventProcessor = new MyEventProcessor();
            mockKafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(topics, mockKafkaEventProducer, eventProcessor);
            MyEventconsumerInterceptorAdaptor myEventconsumerInterceptorAdaptor = new MyEventconsumerInterceptorAdaptor();
            mockKafkaConsumeAndProcess.setInterceptors(myEventconsumerInterceptorAdaptor);
            mockKafkaConsumeAndProcessThread = new Thread(mockKafkaConsumeAndProcess);
            mockKafkaConsumeAndProcessThread.start();


            ///////////////////////////////////////////////////////
            // Normal processing

            final String resourceString = readResourceFile(OBSERVATION_MDC_29112, StandardCharsets.UTF_8);

            Message message = new Message();
            message.setPrefer(Message.Prefer.REPRESENTATION);
            message.setBody(resourceString);
            message.setSender("MockKafkaMessagingTest");
            message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
            message.setBodyCategory(Message.BodyCategory.FHIR);
            message.setBodyType("Observation");
            message.setContentVersion(System.getenv("FHIR_VERSION"));

            ourLog.debug("Sending message to KafkaProducer: " + message.toString());
            //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
            ProducerRecord<String, String> record =
                    new ProducerRecord<>(topic.toString(), message.toString());
            RecordMetadata metadata = mockOurKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
            ourLog.debug("message sent to KafkaProducer: offset = " + metadata.offset());

            assertNotNull(metadata);

            Topic outputTopic = new Topic()
                    .setOperation(Topic.Operation.DataCreated)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation")
                    .addDataCode("MDC29112");

            Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

            Message outputMessage = future.get(10000, TimeUnit.MILLISECONDS);
            Assert.assertTrue(myEventconsumerInterceptorAdaptor.incomingMessagePreParsingCalled);
            Assert.assertTrue(myEventconsumerInterceptorAdaptor.incomingMessagePreProcessMessageCalled);
            Assert.assertTrue(myEventconsumerInterceptorAdaptor.incomingMessagePostProcessMessageCalled);
            Assert.assertFalse(myEventconsumerInterceptorAdaptor.handleExceptionCalled);
            Assert.assertTrue(myEventconsumerInterceptorAdaptor.processingCompletedNormallyCalled);

            ///////////////////////////////////////////////////////
            // Processing with exception in parsing message
            myEventconsumerInterceptorAdaptor = new MyEventconsumerInterceptorAdaptor();
            mockKafkaConsumeAndProcess.setInterceptors(); //clear interceptors before setting new
            mockKafkaConsumeAndProcess.setInterceptors(myEventconsumerInterceptorAdaptor);

            String buggyMsgString = "Not well formated message...";

            ourLog.debug("Sending message to KafkaProducer: " + buggyMsgString);
            //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
            record = new ProducerRecord<>(topic.toString(), buggyMsgString);
            metadata = mockOurKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
            ourLog.debug("message sent to KafkaProducer: offset = " + metadata.offset());

            assertNotNull(metadata);

            outputTopic = new Topic()
                    .setOperation(Topic.Operation.DataCreated)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation")
                    .addDataCode("MDC29112");

            future = mockKafkaEventProducer.getTopicFuture(outputTopic);

            outputMessage = future.get(10000, TimeUnit.MILLISECONDS);
            Assert.assertTrue(myEventconsumerInterceptorAdaptor.incomingMessagePreParsingCalled);
            Assert.assertFalse(myEventconsumerInterceptorAdaptor.incomingMessagePreProcessMessageCalled);
            Assert.assertFalse(myEventconsumerInterceptorAdaptor.incomingMessagePostProcessMessageCalled);
            Assert.assertTrue(myEventconsumerInterceptorAdaptor.handleExceptionCalled);
            Assert.assertFalse(myEventconsumerInterceptorAdaptor.processingCompletedNormallyCalled);
        }
        finally {
            if (mockKafkaConsumeAndProcess != null && mockKafkaConsumeAndProcessThread != null) {
                mockKafkaConsumeAndProcess.stopThread();
                mockKafkaConsumeAndProcessThread.join();
            }
        }

    }

    @Test
    public void testEventProducerInterceptor() throws Exception {
        String messageAsString = readResourceFile(MESSAGE_STRING_FILE, StandardCharsets.UTF_8);
        Message message = MessageParser.parse(messageAsString);

        mockKafkaEventProducer = new MockKafkaEventProducer("MockKafkaMessagingTest");
        MyEventProducerInterceptor eventProducerInterceptor = new MyEventProducerInterceptor();
        mockKafkaEventProducer.setInterceptors(eventProducerInterceptor);

        Topic topic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("testEventProducerInterceptor");

        String savedDataType = topic.getDataType();
        String savedSession = message.getSession();

        mockKafkaEventProducer.sendMessage(topic, message);

        Assert.assertTrue(eventProducerInterceptor.interceptSendCalled);
        Assert.assertTrue(eventProducerInterceptor.interceptSentCalled);
        Assert.assertNotEquals(savedDataType, eventProducerInterceptor.dataTypeBeforeSend);
        Assert.assertEquals(eventProducerInterceptor.dataTypeBeforeSend, eventProducerInterceptor.dataTypeAfterSend);
        Assert.assertNotEquals(savedSession, eventProducerInterceptor.sessionBeforeSend);
        Assert.assertEquals(eventProducerInterceptor.sessionBeforeSend, eventProducerInterceptor.sessionAfterSend);
    }

    @Test
    public void testEventProducerInterceptorNoSend() throws Exception {
        String messageAsString = readResourceFile(MESSAGE_STRING_FILE,  StandardCharsets.UTF_8);
        Message message = MessageParser.parse(messageAsString);

        mockKafkaEventProducer = new MockKafkaEventProducer("MockKafkaMessagingTest");
        MyEventProducerInterceptorNoSend eventProducerInterceptor = new MyEventProducerInterceptorNoSend();
        mockKafkaEventProducer.setInterceptors(eventProducerInterceptor);

        Topic topic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("testEventProducerInterceptor");
        mockKafkaEventProducer.sendMessage(topic, message);

        Assert.assertTrue(eventProducerInterceptor.interceptSendCalled);
        Assert.assertFalse(eventProducerInterceptor.interceptSentCalled);
    }

    @Test
    public void testConsumeAndProcessTopicList()
            throws IOException, InterruptedException, TimeoutException, ExecutionException, ParseException, MessageParseException, KafkaInitializationException, MessagingInitializationException {
        MockKafkaConsumeAndProcess mockKafkaConsumeAndProcess = null;
        Thread mockKafkaConsumeAndProcessThread = null;

        try {
            List<Topic> topics = new ArrayList<>();
            Topic topic = new Topic()
                    .setOperation(Topic.Operation.InputReceived)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation")
                    .addDataCode("MDC29112");
            topics.add(topic);
            mockKafkaEventProducer = new MockKafkaEventProducer("MockKafkaMessagingTest");
            EventProcessor eventProcessor = new MyEventProcessor();
            mockKafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(topics, mockKafkaEventProducer, eventProcessor);
            mockKafkaConsumeAndProcessThread = new Thread(mockKafkaConsumeAndProcess);
            mockKafkaConsumeAndProcessThread.start();

            final String resourceString = readResourceFile(OBSERVATION_MDC_29112, StandardCharsets.UTF_8);

            Message message = new Message();
            message.setPrefer(Message.Prefer.REPRESENTATION);
            message.setBody(resourceString);
            message.setSender("MockKafkaMessagingTest");
            message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
            message.setBodyCategory(Message.BodyCategory.FHIR);
            message.setBodyType("Observation");
            message.setContentVersion(System.getenv("FHIR_VERSION"));

            ourLog.debug("Sending message to KafkaProducer: " + message.toString());
            //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
            ProducerRecord<String, String> record =
                    new ProducerRecord<>(topic.toString(), message.toString());
            RecordMetadata metadata = mockOurKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
            ourLog.debug("message sent to KafkaProducer: offset = " + metadata.offset());

            assertNotNull(metadata);

            Topic outputTopic = new Topic()
                    .setOperation(Topic.Operation.DataCreated)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation")
                    .addDataCode("MDC29112");

            Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

            Message outputMessage = future.get(10000, TimeUnit.MILLISECONDS);
            assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
            assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
            assertEquals("OperationOutcome", outputMessage.getBodyType());
            assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
            assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
            assertEquals(message.getLocation(), outputMessage.getLocation());
        }
        finally {
            if (mockKafkaConsumeAndProcess != null && mockKafkaConsumeAndProcessThread != null) {
                mockKafkaConsumeAndProcess.stopThread();
                mockKafkaConsumeAndProcessThread.join();
            }
        }
    }

    @Test
    public void testConsumeAndProcessTopicPattern()
            throws IOException, InterruptedException, TimeoutException, ExecutionException, ParseException, MessageParseException, KafkaInitializationException, MessagingInitializationException {
        MockKafkaConsumeAndProcess mockKafkaConsumeAndProcess = null;
        Thread mockKafkaConsumeAndProcessThread = null;

        try {
            List<Topic> topics = new ArrayList<>();
            Topic topic = new Topic()
                    .setOperation(Topic.Operation.InputReceived)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation");
            topics.add(topic);
            mockKafkaEventProducer = new MockKafkaEventProducer("MockKafkaMessagingTest");
            EventProcessor eventProcessor = new MyEventProcessor();
            Pattern topicPattern = Pattern.compile(topic.toString() + Topic.separator + "MDC.*");
            mockKafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(topicPattern, mockKafkaEventProducer, eventProcessor);
            mockKafkaConsumeAndProcessThread = new Thread(mockKafkaConsumeAndProcess);
            mockKafkaConsumeAndProcessThread.start();

            final String resourceString = readResourceFile(OBSERVATION_MDC_29112, StandardCharsets.UTF_8);

            Message message = new Message();
            message.setPrefer(Message.Prefer.REPRESENTATION);
            message.setBody(resourceString);
            message.setSender("MockKafkaMessagingTest");
            message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
            message.setBodyCategory(Message.BodyCategory.FHIR);
            message.setBodyType("Observation");
            message.setContentVersion(System.getenv("FHIR_VERSION"));

            ourLog.debug("Sending message to KafkaProducer: " + message.toString());
            //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
            topic.addDataCode("MDC29112");
            ProducerRecord<String, String> record =
                    new ProducerRecord<>(topic.toString(), message.toString());
            RecordMetadata metadata = mockOurKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
            ourLog.debug("message sent to KafkaProducer: offset = " + metadata.offset());

            assertNotNull(metadata);

            Topic outputTopic = new Topic()
                    .setOperation(Topic.Operation.DataCreated)
                    .setDataCategory(Topic.Category.FHIR)
                    .setDataType("Observation")
                    .addDataCode("MDC29112");

            Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

            Message outputMessage = future.get(10000, TimeUnit.MILLISECONDS);
            assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
            assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
            assertEquals("OperationOutcome", outputMessage.getBodyType());
            assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
            assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
            assertEquals(message.getLocation(), outputMessage.getLocation());
        }
        finally {
            if (mockKafkaConsumeAndProcess != null && mockKafkaConsumeAndProcessThread != null) {
                mockKafkaConsumeAndProcess.stopThread();
                mockKafkaConsumeAndProcessThread.join();
            }
        }
    }

  @Test(expected = TimeoutException.class)
  public void testResetHistory()
      throws KafkaInitializationException, MessagingInitializationException, InterruptedException,
      ExecutionException, TimeoutException, ParseException, MessageParseException {
    MockKafkaConsumeAndProcess mockKafkaConsumeAndProcess = null;
    Thread mockKafkaConsumeAndProcessThread = null;

    try {
      List<Topic> topics = new ArrayList<>();
      Topic topic =
          new Topic()
              .setOperation(Topic.Operation.InputReceived)
              .setDataCategory(Topic.Category.FHIR)
              .setDataType("Observation");

      topics.add(topic);
      mockKafkaEventProducer = new MockKafkaEventProducer("MockKafkaMessagingTest");
      EventProcessor eventProcessor = new MyEventProcessor();
      Pattern topicPattern = Pattern.compile(topic.toString() + Topic.separator + "MDC.*");
      mockKafkaConsumeAndProcess =
          new MockKafkaConsumeAndProcess(topicPattern, mockKafkaEventProducer, eventProcessor);
      mockKafkaConsumeAndProcessThread = new Thread(mockKafkaConsumeAndProcess);
      mockKafkaConsumeAndProcessThread.start();

      final String resourceString = "{ zero }";
      topic.addDataCode("MDC29112");
      RecordMetadata metadata = sendMessage(topic, resourceString);
      assertNotNull(metadata);

      Topic outputTopic =
          new Topic()
              .setOperation(Topic.Operation.DataCreated)
              .setDataCategory(Topic.Category.FHIR)
              .setDataType("Observation")
              .addDataCode("MDC29112");

      Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);
      Message outputMessage = future.get(10000, TimeUnit.MILLISECONDS);
      assertNotNull(outputMessage);

      metadata = sendMessage(topic, "{ one }");
      assertNotNull(metadata);

      // Timeout here is important. Without it the future will produce the previous message of the same topic,
      // as the recently sent message would not have reached the mockKafkaEventProducer yet.
      TimeUnit.SECONDS.sleep(1);

      future = mockKafkaEventProducer.getTopicFuture(outputTopic);
      Message outputMessageTwo = future.get(10000, TimeUnit.MILLISECONDS);
      assertNotNull(outputMessageTwo);
      assertNotEquals(outputMessageTwo.getCorrelationId(), outputMessage.getCorrelationId());

      // Reset history should empty all previous messages
      mockKafkaEventProducer.resetHistory();
      future = mockKafkaEventProducer.getTopicFuture(outputTopic);
        // Should time out
        future.get(1000, TimeUnit.MILLISECONDS);
    } finally {
      if (mockKafkaConsumeAndProcess != null && mockKafkaConsumeAndProcessThread != null) {
        mockKafkaConsumeAndProcess.stopThread();
        mockKafkaConsumeAndProcessThread.join();
      }
    }
  }

  private RecordMetadata sendMessage(Topic topic, String resourceString)
      throws InterruptedException, ExecutionException, TimeoutException {

    Message message = new Message();
    message.setPrefer(Message.Prefer.REPRESENTATION);
    message.setBody(resourceString);
    message.setSender("MockKafkaMessagingTest");
    message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
    message.setBodyCategory(Message.BodyCategory.FHIR);
    message.setBodyType("Observation");
    message.setContentVersion(System.getenv("FHIR_VERSION"));

    ourLog.debug("Sending message to KafkaProducer: " + message.toString());
    // Send resource string via KafkaProducer, and wait 10000 ms for Future to return
    ProducerRecord<String, String> record =
        new ProducerRecord<>(topic.toString(), message.toString());
    RecordMetadata metadata = mockOurKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
    ourLog.debug("message sent to KafkaProducer: offset = " + metadata.offset());
    return metadata;
  }


  @AfterClass
    public static void afterClass() throws Exception {
        closeProducers();
    }

    private static void closeProducers() throws InterruptedException {
        ourLog.info("Closing ourKafkaProducer");
        mockOurKafkaProducer.close();
        ourLog.info("Stopping ourKafkaConsumeAndProcess");
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = classLoader.getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        ourLog.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        try {
            Properties properties = new Properties();
            FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
        }
        catch (IOException | IllegalArgumentException e) {
            ourLog.error("Failed loading file from: {}. Error message: {}",  path + "/" + ENVIRONMENT_FILE,
                    e.getMessage());
        }

        //Read KafkaProducer properties and init producer from these
        try (InputStream props = Resources.getResource(KAFKA_PRODUCER_PROPS).openStream()) {
            Properties properties = new Properties();
            properties.load(props);
            Serializer<String> keySerializer = null;
            Serializer<String> valueSerializer = null;

            // Instantiate Kafka Key- and Value-serializers from strings
            try {
                keySerializer =
                        (Serializer<String>) Class.forName(properties.getProperty("key.serializer")).newInstance();
                valueSerializer =
                        (Serializer<String>)Class.forName(properties.getProperty("value.serializer")).newInstance();

                if (keySerializer != null && valueSerializer != null) {
                    mockOurKafkaProducer = new MockProducer<>(true, keySerializer, valueSerializer);
                } else {
                    throw new KafkaInitializationException("KafkaProducer not intialized: either KAFKA_KEY_SERIALIZER or " +
                            "KAFKA_VALUE_SERIALIZER environment variable is incorrect");
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                throw new KafkaInitializationException("KafkaProducer not intialized: The classname found in either " +
                        "KAFKA_KEY_SERIALIZER or KAFKA_VALUE_SERIALIZER environment variable was not found, not " +
                        "accessible or incorrect");
            }
        } catch (IOException | IllegalArgumentException e) {
            ourLog.error("Failed loading file from: {}. Error message: {}",  KAFKA_PRODUCER_PROPS,
                    e.getMessage());
        }
    }
}
