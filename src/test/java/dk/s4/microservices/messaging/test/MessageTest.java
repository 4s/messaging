package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.contrib.java.lang.system.Assertion;

public class MessageTest {

    @Test
    public void preferFromStringNormalTest1() {
        String testString = "return=representation";
        Assert.assertEquals(Message.preferFromString(testString), Message.Prefer.REPRESENTATION);
    }

    @Test
    public void preferFromStringNormalTest2() {
        String testString = "return=OperationOutcome";
        Assert.assertEquals(Message.preferFromString(testString), Message.Prefer.OPERATIONOUTCOME);
    }

    @Test
    public void preferFromStringNormalTest3() {
        String testString = "return=minimal";
        Assert.assertEquals(Message.preferFromString(testString), Message.Prefer.MINIMAL);
    }

    @Test
    public void preferFromStringNullTest1() {
        String testString = "return=";
        Assert.assertNull(Message.preferFromString(testString));
    }

    @Test
    public void preferFromStringNullTest2() {
        String testString = "=";
        Assert.assertNull(Message.preferFromString(testString));
    }

    @Test
    public void preferFromStringNullTest3() {
        String testString = "foo=bar";
        Assert.assertNull(Message.preferFromString(testString));
    }

    @Test
    public void preferFromStringNullTest4() {
        String testString = " ";
        Assert.assertNull(Message.preferFromString(testString));
    }

    @Test
    public void preferFromStringNullTest5() {
        String testString = "";
        Assert.assertNull(Message.preferFromString(testString));
    }

    @Test
    public void getEtagVersionTest1() throws Exception {
        Message m = new Message().setEtag(null);
        Assert.assertNull(m.getEtagVersion());
    }

    @Test
    public void  getEtagVersionTest2() throws Exception {
        Message m = new Message().setEtag("");
        Assert.assertTrue(m.getEtagVersion().isEmpty());
    }
    
    @Test
    public void getEtagVersionTest3() throws Exception {
        Message m = new Message().setEtag("W/\"4232\"");
        Assert.assertTrue(m.getEtagVersion().equals("4232"));
    }

    @Test
    public void getIfMatchVersionTest1() throws Exception {
        Message m = new Message().setIfMatch(null);
        Assert.assertNull(m.getIfMatchVersion());
    }

    @Test
    public void  getIfMatchVersionTest2() throws Exception {
        Message m = new Message().setIfMatch("");
        Assert.assertTrue(m.getIfMatchVersion().isEmpty());
    }

    @Test
    public void getIfMatchVersionTest3() throws Exception {
        Message m = new Message().setIfMatch("W/\"4232\"");
        Assert.assertTrue(m.getIfMatchVersion().equals("4232"));
    }

    @Test
    public void setWeakEtagVersion() {
        Message m = new Message().setWeakEtagVersion("4232");
        Assert.assertTrue(m.getEtag().equals("W/\"4232\""));
        Message m2 = new Message().setWeakEtagVersion("\"4232\"");
        Assert.assertTrue(m2.getEtag().equals("W/\"4232\""));
    }

    @Test
    public void SetWeakIfMatchVersion() {
        Message m = new Message().SetWeakIfMatchVersion("4232");
        Assert.assertTrue(m.getIfMatch().equals("W/\"4232\""));
        Message m2 = new Message().SetWeakIfMatchVersion("\"4232\"");
        Assert.assertTrue(m2.getIfMatch().equals("W/\"4232\""));
    }
}