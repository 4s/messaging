package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.EventConsumerInterceptorAdaptor;
import dk.s4.microservices.messaging.IEventConsumerInterceptor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;

public class MyEventconsumerInterceptorAdaptor implements IEventConsumerInterceptor {
    
    boolean incomingMessagePreParsingCalled = false;
    boolean incomingMessagePreProcessMessageCalled = false;
    boolean incomingMessagePostProcessMessageCalled = false;
    boolean handleExceptionCalled = false;
    boolean processingCompletedNormallyCalled = false;

    @Override
    public boolean incomingMessagePreParsing(String consumedTopic, String receivedMessage) {
        incomingMessagePreParsingCalled = true;
        return true;
    }

    @Override
    public boolean incomingMessagePreProcessMessage(Topic consumedTopic, Message receivedMessage) {
        incomingMessagePreProcessMessageCalled = true;
        return true;
    }

    @Override
    public boolean incomingMessagePostProcessMessage(Topic consumedTopic, Message receivedMessage) {
        incomingMessagePostProcessMessageCalled = true;
        return true;
    }

    @Override
    public boolean handleException(String consumedTopic, String receivedMessage, Exception exception) {
        handleExceptionCalled = true;
        return true;
    }

    @Override
    public void processingCompletedNormally(Topic consumedTopic, Message receivedMessage) {
        processingCompletedNormallyCalled = true;
    }
}
