package dk.s4.microservices.messaging.test;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import com.google.common.io.Resources;
import dk.s4.microservices.messaging.*;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentMatcher;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

public class KafkaMessagingIT {

    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(KafkaMessagingIT.class);

	private static int ourPort;

    private static KafkaProducer<String, String> ourKafkaProducer;
    private static KafkaConsumeAndProcess ourKafkaConsumeAndProcess;
    private static Thread ourKafkaConsumeAndProcessThread;

    private static String ENVIRONMENT_FILE = "kafka.env";
	private static String KAFKA_PRODUCER_PROPS = "KafkaProducer.props";

    private static String OBSERVATION_MDC_29112 = "ObservationMDC29112.json";
	private static String INPUT_TOPIC_MDC_29112 = "InputReceived_FHIR_Observation_MDC29112";
    private static String OUTPUT_TOPIC_MDC_29112 = "DataCreated_FHIR_Observation_MDC29112";

	@ClassRule
	public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    @Test
    public void testConsumeAndProcessTopicList()
            throws IOException, InterruptedException, TimeoutException, ExecutionException, KafkaInitializationException, MessagingInitializationException {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        final Appender mockAppender = mock(Appender.class);
        when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

        KafkaEventProducer kafkaEventProducer = new KafkaEventProducer(System.getenv("SERVICE_NAME"));

        //KafkaConsumeAndProcess based on list of Topics:
        List<Topic> topics = new ArrayList<>();
        Topic topic = new Topic()
                .setOperation(Topic.Operation.InputReceived)
                .setDataCategory(Topic.Category.FHIR)
                .setDataType("Observation")
                .addDataCode("MDC29112");
        topics.add(topic);
        EventProcessor eventProcessor = new MyEventProcessor();
        ourKafkaConsumeAndProcess = new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor);
        ourKafkaConsumeAndProcessThread = new Thread(ourKafkaConsumeAndProcess);
        ourKafkaConsumeAndProcessThread.start();


        //Read resource from file
        String filepath = KafkaMessagingIT.class.getClassLoader().getResource(OBSERVATION_MDC_29112).toString();
        final String resourceString = readFile(filepath.replace("file:", ""), StandardCharsets.UTF_8);

        Message message = new Message();
        message.setBody(resourceString);
        message.setSender(System.getenv("SERVICE_NAME"));
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(Message.BodyCategory.FHIR);
        message.setBodyType("Observation");
        message.setContentVersion(System.getenv("FHIR_VERSION"));
        //Prefer is actually only valid when issuing Create or Update, but here we just put it in the header
        // to test that having it here doesn't mess with the sending and reception of the message:
        message.setPrefer(Message.Prefer.OPERATIONOUTCOME);

        ourLog.debug("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        ProducerRecord<String, String> record =
                new ProducerRecord<String, String>(topic.toString(), message.toString());
        RecordMetadata metadata = ourKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        ourLog.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        assertNotNull(metadata);

        // Thread.sleep is a hack to have Kafka in other threads come
        // back with results.
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
            @Override
            public boolean matches(final Object argument) {
                return ((LoggingEvent) argument).getFormattedMessage().contains("EventConsumer processing: topic = " + INPUT_TOPIC_MDC_29112)
                        &&
                        ((LoggingEvent) argument).getFormattedMessage().contains(message.toString());
            }
        }));
        verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
            @Override
            public boolean matches(final Object argument) {
                return ((LoggingEvent) argument).getFormattedMessage().contains("sendMessage: Message sent to topic = " + OUTPUT_TOPIC_MDC_29112);
            }
        }));
    }

    @AfterClass
	public static void afterClass() throws Exception {
        closeProducersAndConsumers();
    }

    private static void closeProducersAndConsumers() throws InterruptedException {
        ourLog.info("Closing ourKafkaProducer");
        ourKafkaProducer.close();
        ourLog.info("Stopping ourKafkaConsumeAndProcess");
        ourKafkaConsumeAndProcess.stopThread();
        ourKafkaConsumeAndProcessThread.join();
    }

    @BeforeClass
	public static void beforeClass() throws Exception {
		/*
		 * This runs under maven, and I'm not sure how else to figure out the target directory from code..
		 */
		String path = KafkaMessagingIT.class.getClassLoader().getResource(".keep").getPath();
		path = new File(path).getParent();
		path = new File(path).getParent();
		path = new File(path).getParent();

		ourLog.info("Project base path is: {}", path);

		//Read and set environment variables from .env file:
		try {
			Properties properties = new Properties();
			FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
			properties.load(environmentFile);
			properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
		}
		catch (IOException | IllegalArgumentException e) {
			ourLog.error("Failed loading file from: {}. Error message: {}",  path + "/" + ENVIRONMENT_FILE,
					e.getMessage());
		}

		//Read KafkaProducer properties and init producer from these
        try (InputStream props = Resources.getResource(KAFKA_PRODUCER_PROPS).openStream()) {
            Properties properties = new Properties();
            properties.load(props);
            ourKafkaProducer = new KafkaProducer<>(properties);
        } catch (IOException | IllegalArgumentException e) {
            ourLog.error("Failed loading file from: {}. Error message: {}",  KAFKA_PRODUCER_PROPS,
                    e.getMessage());
        }
    }
}
