package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.IEventProducerInterceptor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;

public class MyEventProducerInterceptor implements IEventProducerInterceptor {

    boolean interceptSendCalled = false;
    boolean interceptSentCalled = false;
    String dataTypeBeforeSend, dataTypeAfterSend;
    String sessionBeforeSend, sessionAfterSend;


    @Override
    public boolean interceptSend(Topic topic, Message message) {
        interceptSendCalled = true;

        topic.setDataType("interceptSend");
        message.setSession("interceptSend");

        dataTypeBeforeSend = topic.getDataType();
        sessionBeforeSend = message.getSession();

        return true;
    }

    @Override
    public void interceptSent(Topic topic, Message message) {
        interceptSentCalled = true;

        dataTypeAfterSend = topic.getDataType();
        sessionAfterSend = message.getSession();
    }
}
