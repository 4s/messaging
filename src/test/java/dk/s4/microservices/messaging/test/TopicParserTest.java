package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.TopicParseException;
import dk.s4.microservices.messaging.TopicParser;
import org.junit.Test;

import static org.junit.Assert.*;

public class TopicParserTest {

    private final String TOPIC1 = "InputReceived_FHIR_Observation_MDC188736";
    private final String TOPIC2 = "InputReceived_FHIR_Bundle_MDC188736_MDC150020_MDC149546";
    private final String TOPIC3 = "Create_FHIR_Questionnaire";

    @Test
    public void parseTopic1() throws TopicParseException {
        Topic topic = TopicParser.parse(TOPIC1);
        assertEquals(Topic.Operation.InputReceived, topic.getOperation());
        assertEquals(Topic.Category.FHIR, topic.getDataCategory());
        assertTrue(topic.getDataType().equals("Observation"));
        assertEquals(1, topic.getDataCodes().size());
        assertTrue(topic.getDataCodes().contains("MDC188736"));
    }

    @Test
    public void parseTopic2() throws TopicParseException {
        Topic topic = TopicParser.parse(TOPIC2);
        assertEquals(Topic.Operation.InputReceived, topic.getOperation());
        assertEquals(Topic.Category.FHIR, topic.getDataCategory());
        assertTrue(topic.getDataType().equals("Bundle"));
        assertEquals(3, topic.getDataCodes().size());
        assertTrue(topic.getDataCodes().contains("MDC188736"));
        assertTrue(topic.getDataCodes().contains("MDC150020"));
        assertTrue(topic.getDataCodes().contains("MDC149546"));
    }

    @Test
    public void parseTopic3() throws TopicParseException {
        Topic topic = TopicParser.parse(TOPIC3);
        assertEquals(Topic.Operation.Create, topic.getOperation());
        assertEquals(Topic.Category.FHIR, topic.getDataCategory());
        assertTrue(topic.getDataType().equals("Questionnaire"));
        assertTrue(topic.getDataCodes().isEmpty());
    }
}