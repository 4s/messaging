package dk.s4.microservices.messaging.test;

import dk.s4.microservices.messaging.IEventProducerInterceptor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;

public class MyEventProducerInterceptorNoSend implements IEventProducerInterceptor {

    boolean interceptSendCalled = false;
    boolean interceptSentCalled = false;

    @Override
    public boolean interceptSend(Topic topic, Message message) {
        interceptSendCalled = true;

        return false; //Don't send message
    }

    @Override
    public void interceptSent(Topic topic, Message message) {
        interceptSentCalled = true; //Should never get called
    }
}
