package dk.s4.microservices.messaging;

public class MessageIncompatibleException extends Exception {
    public MessageIncompatibleException() {
    }

    public MessageIncompatibleException(String message) {
        super(message);
    }

    public MessageIncompatibleException(Throwable cause) {
        super(cause);
    }

    public MessageIncompatibleException(String message, Throwable cause) {
        super(message, cause);
    }
}
