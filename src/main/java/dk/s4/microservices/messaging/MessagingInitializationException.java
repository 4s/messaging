package dk.s4.microservices.messaging;

public class MessagingInitializationException extends Exception {
    public MessagingInitializationException() {
    }

    public MessagingInitializationException(String message) {
        super(message);
    }

    public MessagingInitializationException(Throwable cause) {
        super(cause);
    }

    public MessagingInitializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
