package dk.s4.microservices.messaging;

public class TopicParseException extends Exception {
    public TopicParseException() {
    }

    public TopicParseException(String message) {
        super(message);
    }

    public TopicParseException(Throwable cause) {
        super(cause);
    }

    public TopicParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
