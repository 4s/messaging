package dk.s4.microservices.messaging;

/**
 * Provides methods to intercept in- and outgoing messages processed by instances of {@link EventConsumer}.
 * Implementations of this interface may wish to use {@link EventConsumerInterceptorAdaptor} in order to not need to
 * implement every method.
 *
 *
 *
 * <p>
 * This class is heavily inspired by HAPI FHIR server interceptors (<a href="http://jamesagnew.github.io/hapi-fhir/doc_rest_server_interceptor.html">HAPI FHIR server interceptor documentation</a>).
 * </p>
 */
public interface IEventConsumerInterceptor {


    /**
     * This method is called before any other processing takes place for each incoming message. It may be e.g. be used
     * to provide alternate processing of incoming messages.
     *
     * @param consumedTopic Unparsed Topic <code>String</code> that the incoming message was received on
     * @param receivedMessage Unparsed incomming message <code>String</code>
     *
     * @return Return <code>true</code> if processing should continue normally. If your interceptor is providing
     *   processing and its own response rather than letting the EventConsumer handle the response normally, you must
     *   return <code>false</code>. In this case, no further processing will occur and no further interceptors will be
     *   called.
     */
    boolean incomingMessagePreParsing(String consumedTopic, String receivedMessage);

    /**
     * This method is called for each message, immediately after parsing of incoming topic and message strings and
     * before any other processing takes place for each incoming message. It may e.g. be used to provide
     * alternate processing of incoming messages.
     *
     * @param consumedTopic {@link Topic} that the incoming message was received on
     * @param receivedMessage Incoming {@link Message}

     * @return Return <code>true</code> if processing should continue normally. If your interceptor is providing
     *   processing and its own response rather than letting the EventConsumer handle the response normally, you must
     *   return <code>false</code>. In this case, no further processing will occur and no further interceptors will be
     *   called.
     */
    boolean incomingMessagePreProcessMessage(Topic consumedTopic, Message receivedMessage);

    /**
     * This method is called for each message, immediately after processing the incoming message, but before any
     * response is sent out be the {@link EventConsumer}. It may e.g. be used to provide alternate responses to
     * incoming messages.
     *
     * @param consumedTopic {@link Topic} that the incoming message was received on
     * @param receivedMessage Incoming {@link Message}

     * @return Return <code>true</code> if response(s) should be sent as normally. If your interceptor is providing a
     *   response rather than letting the EventConsumer handle the response normally, you must return <code>false</code>.
     *   In this case, no further processing will occur and no further interceptors will be called.
     */
    boolean incomingMessagePostProcessMessage(Topic consumedTopic, Message receivedMessage);

    /**
     * This method is called upon any exception being thrown within the {@link EventConsumer}'s processing code.
     * This includes parse exceptions and message incompatibility exceptions.
     *
     * @param consumedTopic Unparsed Topic <code>String</code> that the incoming message was received on
     * @param receivedMessage Unparsed incomming message <code>String</code>
     * @param exception Exception thrown from {@link EventConsumer}
     *
     * @return Return <code>true</code> if processing should continue normally.
     *   If your interceptor is providing its own response rather than letting the EventConsumer handle the response
     *   normally, you must return <code>false</code>. In this case, no further processing will occur and no further
     *   interceptors will be called.
     */
    boolean handleException(String consumedTopic, String receivedMessage, Exception exception);

    /**
     * This method is called after all processing of an incoming message is completed, but only if the
     * request completes normally (i.e. no exception is thrown).
     *
     * @param consumedTopic {@link Topic} that the incoming message was received on
     * @param receivedMessage Incoming {@link Message}
     */
    void processingCompletedNormally(Topic consumedTopic, Message receivedMessage);
}
