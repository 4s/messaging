package dk.s4.microservices.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class ReplyEventProcessor implements EventProcessor {

    private final static Logger logger = LoggerFactory.getLogger(ReplyEventProcessor.class);

    private ConcurrentHashMap<String, ReplyFuture> replyFutures = new ConcurrentHashMap<>();

    public ReplyFuture addReplyFuture(String transactionId) {
        logger.trace("Adding ReplyFuture for transactionId = " + transactionId);
        ReplyFuture replyFuture = new ReplyFuture();
        replyFutures.put(transactionId, replyFuture);
        return replyFuture;
    }

    public void removeReplyFuture(String transactionId) {
        logger.trace("Removing ReplyFuture for transactionId = " + transactionId);
        replyFutures.remove(transactionId);
    }

    @Override
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage) {

        if (replyFutures.containsKey(receivedMessage.getTransactionId())) {
            logger.trace("Got reply on future with transactionId = " + receivedMessage.getTransactionId());
            replyFutures.get(receivedMessage.getTransactionId()).put(receivedMessage);

            return true;
        }

        return false;
    }
}
