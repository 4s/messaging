package dk.s4.microservices.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TopicParser {
    private final static Logger logger = LoggerFactory.getLogger(TopicParser.class);

    /**
     * Will parse topic strings on the form below and return a corresponding Topic object:
     *
     * Topic = Event, "_", DataCategory, "_", DataType, { "_", DataCode }
     * (EBNF notation)
     *
     *
     * @param topicString String to parse
     * @return
     */
    public static Topic parse(String topicString) throws TopicParseException {
        if (topicString == null)
            throw new TopicParseException("Topic string is null");
        if (topicString.isEmpty())
            throw new TopicParseException("Topic string is empty");

        Topic topic = new Topic();

        String[] parts = topicString.split(Topic.separator);
        if (parts.length < 1)
            throw new TopicParseException("Topic string " + topicString + " is ill formated");

        int pos = 0;
        for (String part : parts) {
            if (pos == 0)
                topic.setOperation(Topic.operationFromString(part));
            if (pos == 1)
                topic.setDataCategory(Topic.categoryFromString(part));
            if (pos == 2)
                topic.setDataType(part);
            if (pos >= 3)
                topic.addDataCode(part);
            pos++;
        }
        return topic;
    }
}
