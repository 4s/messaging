package dk.s4.microservices.messaging;

public interface EventProcessor {
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage);
}
