package dk.s4.microservices.messaging;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventOperationOutcome {
    public static final String FATAL = "fatal";
    public static final String ERROR = "error";
    public static final String WARNING = "warning";
    public static final String INFORMATION = "information";

    private final static Logger logger = LoggerFactory.getLogger(EventOperationOutcome.class);

    public EventOperationOutcome(String severity, String code, String diagnostics) {
        setSeverity(severity);
        setCode(code);
        setDiagnostics(diagnostics);
    }

    public EventOperationOutcome(String severity, String diagnostics) {
        setSeverity(severity);
        setDiagnostics(diagnostics);
    }

    private String severity;
    private String code;
    private String diagnostics;

    public String getSeverity() {
        return severity;
    }

    public EventOperationOutcome setSeverity(String severity) {
        this.severity = severity;
        return this;
    }

    public String getCode() {
        return code;
    }

    public EventOperationOutcome setCode(String code) {
        this.code = code;
        return this;
    }

    public String getDiagnostics() {
        return diagnostics;
    }

    public EventOperationOutcome setDiagnostics(String diagnostics) {
        this.diagnostics = diagnostics;
        return this;
    }

    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("resourceType", "OperationOutcome");
        JSONArray issues = new JSONArray();
        JSONObject issue = new JSONObject();
        if (getSeverity() != null) issue.put("severity", getSeverity());
        if (getCode() != null) issue.put("code", getCode());
        if (getDiagnostics() != null) issue.put("diagnostics", getDiagnostics());
        issues.add(issue);
        jsonObject.put("issue", issues);

        return jsonObject.toString();
    }
}
