package dk.s4.microservices.messaging;

public class Version {
    public Integer major, minor, patch;

    public Version(Integer major, Integer minor, Integer patch) {
        this.major = major;
        this.minor = minor;
        this.patch = patch;
    }

    public Version(String versionString) {
        String[] strings = versionString.split("\\.");
        major = Integer.parseInt(strings[0]);
        minor = Integer.parseInt(strings[1]);
        patch = Integer.parseInt(strings[2]);
    }

    public boolean isCompatible(Version version) {
        if (this.major == version.major)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return major + "." + minor + "." + patch;
    }
}

