package dk.s4.microservices.messaging;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for producing topics on the form (in EBNF notation):
 *
 * Topic = Event, "_", DataCategory, "_", DataType, { "_", DataCode }
 *
 * If the data type has a code on root level (like FHIR Observation and DeviceComponent have), then this code is added
 * as the DataCode part. Example:
 *
 * InputReceived_FHIR_Observation_MDC29112
 *
 * If the data type does not have a code on root level (like FHIR Patient), then there's no DataCode in the topic:
 *
 * InputReceived_FHIR_Patient
 *
 */
public class Topic {
    private Operation operation = null;
    private Category dataCategory = null;
    private String dataType = "";
    private List<String> dataCodes = new ArrayList<>();

    public Topic() {}

    /**
     * Copy constructur. Instantiates this Topic as a copy of other
     *
     * @param other Topic to copy
     */
    public Topic(Topic other) {
        this.setOperation(other.getOperation());
        this.setDataCategory(other.getDataCategory());
        this.setDataType(other.getDataType());
        this.setDataCodes(other.getDataCodes());
    }

    public static final String separator = "_";

    // Below Topic constants are defined in enum's.

    //Operation enum defines allowed events and commands
    public enum Operation {
        //Events:
        InputReceived , DataValidated , DataConverted , DataCreated , DataUpdated , TransactionCompleted ,
        ProcessingOk , ProcessingFailed , Error,
        //Commands:
        Create, Update, Delete
    }

    //Category enum
    public enum Category {
        FHIR, CDA, System
    }

    /**
     * Parses a String into an {@link Operation} value
     *
     * @param opString {@link Operation} as String
     * @return {@link Operation} value matching the input String. If no match is found null is returned.
     */
    public static Operation operationFromString(String opString) {
        if (Operation.InputReceived.name().equals(opString))
            return Operation.InputReceived;
        if (Operation.DataValidated.name().equals(opString))
            return Operation.DataValidated;
        if (Operation.DataConverted.name().equals(opString))
            return Operation.DataConverted;
        if (Operation.DataCreated.name().equals(opString))
            return Operation.DataCreated;
        if (Operation.DataUpdated.name().equals(opString))
            return Operation.DataUpdated;
        if (Operation.TransactionCompleted.name().equals(opString))
            return Operation.TransactionCompleted;
        if (Operation.ProcessingOk.name().equals(opString))
            return Operation.ProcessingOk;
        if (Operation.ProcessingFailed.name().equals(opString))
            return Operation.ProcessingFailed;
        if (Operation.Error.name().equals(opString))
            return Operation.Error;
        if (Operation.Create.name().equals(opString))
            return Operation.Create;
        if (Operation.Update.name().equals(opString))
            return Operation.Update;
        if (Operation.Delete.name().equals(opString))
            return Operation.Delete;
        else
            return null;
    }

    /**
     * Parses a String into a {@link Category} value
     *
     * @param catString {@link Category} as String
     * @return {@link Category} value matching the input String. If no match is found null is returned.
     */
    public static Category categoryFromString(String catString) {
        if (Category.FHIR.name().equals(catString))
            return Category.FHIR;
        if (Category.CDA.name().equals(catString))
            return Category.CDA;
        if (Category.System.name().equals(catString))
            return Category.System;
        else
            return null;
    }

    public Operation getOperation() {
        return operation;
    }

    public Topic setOperation(Operation operation) {
        this.operation = operation;
        return this;
    }

    public Category getDataCategory() {
        return dataCategory;
    }

    public Topic setDataCategory(Category dataCategory) {
        this.dataCategory = dataCategory;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public Topic setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public List<String> getDataCodes() {
        return dataCodes;
    }

    public Topic setDataCodes(List<String> dataCodes) {
        this.dataCodes = dataCodes;
        return this;
    }

    public Topic addDataCode(String dataCode) {
        this.dataCodes.add(dataCode);
        return this;
    }

    public String toString() {
        String topic = "";
        if (getOperation() != null) {
            topic = getOperation().name();
            if (getDataCategory() != null) {
                topic = topic + separator + getDataCategory().name();
                if (!getDataType().isEmpty()) {
                    topic = topic + separator + getDataType();
                    for (String dataCode : getDataCodes())
                        topic = topic + separator + dataCode;
                }
            }
        }

        return topic;
    }
}
