package dk.s4.microservices.messaging.mock;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serializer;

import java.util.concurrent.Future;

/**
 * The MockProducer is a subclass of kafka's Mockproducer, which produces messages to the MockKafkaDatastore and
 * otherwise executes the superclass' methods.
 */
public class MockProducer<K, V> extends org.apache.kafka.clients.producer.MockProducer<K, V> {

    private MockKafkaDatastore<K, V> store;

    /**
     * Constructor loads properties from environment, retrieves a MockKafkaDatastore instance and instantiates internal
     * MockKafkaProducer.
     *
     * @param autoComplete whether the send request should be automatically completed
     * @param keySerializer serializer for keys
     * @param valueSerializer serializer for values
     */
    public MockProducer(boolean autoComplete, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        super(autoComplete, keySerializer, valueSerializer);
        store = (MockKafkaDatastore<K, V>) MockKafkaDatastore.getInstance();
    }


    /**
     * Sends message to MockKafkaDatastore and executes the superclass' send-method
     * @param producerRecord the data to be sent to the MockKafkaDatastore
     * @param callback the callback-method to be called upon completion
     * @return a future containing metadata about the request
     */
    @Override
    public Future<RecordMetadata> send(ProducerRecord<K, V> producerRecord, Callback callback) {
        store.send(producerRecord);
        return super.send(producerRecord, callback);
    }
}
