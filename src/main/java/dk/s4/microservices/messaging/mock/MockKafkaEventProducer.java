package dk.s4.microservices.messaging.mock;

import dk.s4.microservices.messaging.*;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import java.util.List;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.AuthenticationException;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.errors.TimeoutException;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.*;

import static net.logstash.logback.argument.StructuredArguments.v;

/**
 * The MockKafkaEventProducer writes messages to a topic on the MockKafkaDatastore.
 */
public class MockKafkaEventProducer extends EventProducer {

	private final Logger logger = LoggerFactory.getLogger(MockKafkaEventProducer.class);
	private MockProducer<String, String> mockProducer;

	/**
	 * Constructor loads properties from environment and instantiates internal MockKafkaProducer.
	 *
	 * @param sender The sender of messages.
	 * @throws KafkaInitializationException
	 */
	public MockKafkaEventProducer(String sender) throws KafkaInitializationException {
		super(sender);
		Properties props = loadProducerPropertiesFromEnv();

		Serializer<String> keySerializer;
		Serializer<String> valueSerializer;

		// Instantiate Kafka Key- and Value-serializers from strings
		try {
			keySerializer =
					(Serializer<String>) Class.forName(props.getProperty("key.serializer")).newInstance();
			valueSerializer =
					(Serializer<String>)Class.forName(props.getProperty("value.serializer")).newInstance();

			if (keySerializer != null && valueSerializer != null) {
				mockProducer = new MockProducer<>(true, keySerializer, valueSerializer);
			} else {
				throw new KafkaInitializationException("KafkaProducer not intialized: either KAFKA_KEY_SERIALIZER or " +
						"KAFKA_VALUE_SERIALIZER environment variable is incorrect");
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			throw new KafkaInitializationException("KafkaProducer not intialized: The classname found in either " +
					"KAFKA_KEY_SERIALIZER or KAFKA_VALUE_SERIALIZER environment variable was not found or not accessible");
		}
	}

	/**
	 * Sends message to Kafka on given topic to MockKafkaDatastore.
     *
	 * @param topic Destination Kafka topic.
	 * @param message Message to send to Kafka.
	 * @throws AuthenticationException
	 * @throws IllegalStateException
	 * @throws SerializationException
	 * @throws TimeoutException
	 * @throws KafkaException
	 */
	protected void doSendMessage(final Topic topic, Message message) throws AuthenticationException,
	IllegalStateException, SerializationException, TimeoutException, KafkaException {
		Map<String,String> mdcContextMap = MDC.getCopyOfContextMap();
		message.setSender(sender);
		ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic.toString(), message.toString());
		mockProducer.send(record,
				new Callback() {
					public void onCompletion(RecordMetadata metadata, Exception e) {
						if (mdcContextMap != null)
							MDC.setContextMap(mdcContextMap); //ensure callback thread has same MDC context
						else
							logger.warn("MDC context map is null");
						if(e != null) {
							logger.error("Error during sendMessageToKafka: "+ e.getMessage(), e);
							e.printStackTrace();
						} else {
							logger.trace("sendMessage: Message sent to topic = {} key = {} " +
											"meta(partition = {}, offset = {}) + message = {}\n",
									v("messaging_topic", record.topic()),
									v("kafka_key", record.key()),
									v("kafka_partition", metadata.partition()),
									v("kafka_offset", metadata.offset()),
									v("messaging_message", record.value()));
						}
					}
				});
	}

	private Properties loadProducerPropertiesFromEnv() throws KafkaInitializationException {
		//Get KafkaProducer configuration from environment
		Properties properties = new Properties();
		String kafka_bootstrap_server = System.getenv("KAFKA_BOOTSTRAP_SERVER");
		if (kafka_bootstrap_server != null) {
			properties.put("bootstrap.servers", kafka_bootstrap_server);

			String kafka_key_serializer = System.getenv("KAFKA_KEY_SERIALIZER");
			String kafka_value_serializer = System.getenv("KAFKA_VALUE_SERIALIZER");
			if (kafka_key_serializer != null && kafka_value_serializer != null) {
				properties.put("key.serializer", kafka_key_serializer);
				properties.put("value.serializer", kafka_value_serializer);

				String kafka_acks = System.getenv("KAFKA_ACKS");
				if (kafka_acks == null)
					logger.warn("KafkaProducer initialization: KAFKA_ACKS not specified (Kafka defaults to acks=1)");
				else
					properties.put("acks", kafka_acks);
				String kafka_retries = System.getenv("KAFKA_RETRIES");
				if (kafka_retries == null)
					logger.warn("KafkaProducer initialization: KAFKA_RETRIES not specified (Kafka defaults to retries=0)");
				else
					properties.put("retries", kafka_retries);
			}
			else
				throw new KafkaInitializationException("KafkaProducer not intialized: Either KAFKA_KEY_SERIALIZER or KAFKA_VALUE_SERIALIZER (or both) is undefined!");
		}
		else
			throw new KafkaInitializationException("KafkaProducer not intialized: KAFKA_BOOTSTRAP_SERVER is undefined!");

		return properties;
	}

	public Future<Message> getTopicFuture(Topic topic) throws ParseException, MessageParseException {
		Callable<Message> callable = () -> {
			Message m = null;
			while(m == null) {
				if (!mockProducer.history().isEmpty()) {
					for (ProducerRecord<String, String> record: mockProducer.history()) {
						if (record.topic().equals(topic.toString())) {
							m = MessageParser.parse(record.value());
						}
					}
				}
				Thread.sleep(100);
			}
			return m;
		};
		ExecutorService threadPool = Executors.newFixedThreadPool(1);
		Future<Message> future = threadPool.submit(callable);
		threadPool.shutdown();
		return future;
	}

  public void resetHistory() {
	  mockProducer.clear();
  }
}
