package dk.s4.microservices.messaging.mock;

import dk.s4.microservices.messaging.EventConsumer;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import java.util.regex.Pattern;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.consumer.InvalidOffsetException;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static net.logstash.logback.argument.StructuredArguments.v;

/**
 * Class for consuming, processing and responding to messages over Kafka.
 */
public class MockKafkaConsumeAndProcess extends EventConsumer implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(MockKafkaConsumeAndProcess.class);

    private volatile boolean running;

    private MockConsumer<String, String> mockConsumer;

    /**
     * Constructor loads properties from environment and instantiates internal KafkaConsumer.
     *
     * @param topics Kafka topics to subscribe to.
     * @param eventProducer MockKafkaEventProducer used for responding to messages (result and/or status of processing)
     * @param eventProcessor Instance of implementation of {@link EventProcessor}. The
     *                       EventProcessor implementation implements the main processing invoked on reception of
     *                       message.
     * @throws MessagingInitializationException
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     */
    public MockKafkaConsumeAndProcess(List<Topic> topics,
                                      MockKafkaEventProducer eventProducer,
                                      EventProcessor eventProcessor)
            throws MessagingInitializationException, IllegalArgumentException,
            IllegalStateException, KafkaException {
        super(topics, eventProducer, eventProcessor);

        mockConsumer = new MockConsumer<>(OffsetResetStrategy.EARLIEST);

        List<String> topicStrings = new ArrayList<>();
        for (Topic topic: topics)
            topicStrings.add(topic.toString());
        mockConsumer.subscribe(topicStrings);
    }

    /**
     * Constructor loads properties from environment and instantiates internal KafkaConsumer.
     *
     * @param topicPattern Subscribe to all Kafka topics matching specified pattern.
     * @param eventProducer KafkaEventProducer used for responding to messages (result and/or status of processing)
     * @param eventProcessor Instance of implementation of {@link dk.s4.microservices.messaging.EventProcessor}. The
     *                       EventProcessor implementation implements the main processing invoked on reception of
     *                       message.
     * @throws MessagingInitializationException
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     * @throws KafkaException
     */
    public MockKafkaConsumeAndProcess(Pattern topicPattern,
            MockKafkaEventProducer eventProducer,
            EventProcessor eventProcessor)
            throws MessagingInitializationException, IllegalArgumentException,
            IllegalStateException, KafkaException {
        super(topicPattern, eventProducer, eventProcessor);

        mockConsumer = new MockConsumer<>(OffsetResetStrategy.EARLIEST);

        mockConsumer.subscribe(topicPattern);
    }

    @Override
    public void run() {
        logger.info("Starting KafkaConsumeAndProcessThread");
        running = true;
        while (running) {
            try {
                ConsumerRecords<String, String> records = mockConsumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    logger.trace("MockKafkaConsumeAndProcess processing: topic = {}, message = {} offset = {}",
                            v("messaging_topic", record.topic()),
                            v("messaging_message", record.value()),
                            v("kafka_offset", record.offset()));

                    try {
                        doProcessMessage(record.topic(), record.value());
                    } catch (IllegalStateException | KafkaException e) {
                        logger.error("Error sending message to Kafka: "+ e.getMessage(), e);
                    }

                    try {
                        logger.debug("kafkaConsumer.commitSync()");
                        mockConsumer.commitSync(); //Commit offset after write (or not) to db and after telling the world about it
                        logger.debug("kafkaConsumer.commitSync() DONE");
                    } catch (KafkaException e) {
                        logger.error("Error commiting offset to Kafka: "+ e.getMessage(), e);
                    }

                    MDC.clear();
                }
            } catch (IllegalArgumentException | IllegalStateException | KafkaException e) {
                logger.error("Error polling Kafka topics {}: {}", topicsString, e.getMessage());
                logger.error("Exception polling Kafka topics"+ e.getMessage(), e);
            }
        }
        logger.debug("Closing kafkaConsumer");
        mockConsumer.close();
    }

    public void stopThread() {
        running = false;
    }
}
