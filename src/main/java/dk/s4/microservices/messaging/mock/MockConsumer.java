package dk.s4.microservices.messaging.mock;

import java.util.regex.Pattern;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;

import java.util.Collection;

/**
 * The MockConsumer is a subclass of kafka's MockConsumer, which consumes messages from the MockKafkaDatastore and
 * otherwise executes the superclass' methods.
 */
public class MockConsumer<K, V> extends org.apache.kafka.clients.consumer.MockConsumer<K, V> {

    private MockKafkaDatastore<K, V> store;
    private Collection<String> topics;
    private Pattern topicPattern;

    /**
     * Constructor retrieves a MockKafkaDatastore instance and instantiates internal MockConsumer.
     * @param offsetResetStrategy the offset reset strategy to be used
     */
    MockConsumer(OffsetResetStrategy offsetResetStrategy) {
        super(offsetResetStrategy);
        store = (MockKafkaDatastore<K, V>) MockKafkaDatastore.getInstance();
    }

    /**
     * Subscribes the MockConsumer to topics.
     * @param topics the topics to subscribe to
     */
    @Override
    public void subscribe(Collection<String> topics) {
        super.subscribe(topics);
        this.topics = topics;
    }

    /**
     * Subscribes the MockConsumer to topic pattern.
     * @param topicPattern the topic pattern to subscribe to
     */
    @Override
    public void subscribe(Pattern topicPattern) {
        super.subscribe(topicPattern);
        this.topicPattern = topicPattern;
    }

    /**
     * Polls the MockKafkaDatastore for messages from the topics the MockConsumer has subscribed to.
     * @param timeout timeout in ms
     * @return A Collection of consumerrecords found in the MockKafkaDatastore
     */
    @Override
    public ConsumerRecords<K, V> poll(long timeout) {
        super.poll(timeout);
        if (topics != null)
            return store.poll(topics);
        else if (topicPattern != null)
            return store.poll(topicPattern);
        else
            return null; //should not happen, can't throw exception here as this would change method signature
    }
}
