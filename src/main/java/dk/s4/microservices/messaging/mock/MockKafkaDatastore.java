package dk.s4.microservices.messaging.mock;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import static net.logstash.logback.argument.StructuredArguments.v;

class MockKafkaDatastore<K, V> {

    private final static Logger logger = LoggerFactory.getLogger(MockKafkaDatastore.class);
    private HashMap<String, LinkedBlockingQueue<Pair<K, V>>> store;
    private static MockKafkaDatastore<?, ?> instance = null;

    /**
     * Constructor instantiates the store hashmap
     */
    private MockKafkaDatastore() {
        store = new HashMap<>();
    }

    /**
     * Singleton
     * @return either an existing instance of the MockKafkaDatastore, or a new instance if an instance has not already
     * been instantiated
     */
    static MockKafkaDatastore<?, ?> getInstance() {
        if(instance == null) {
            instance = new MockKafkaDatastore();
        }
        return instance;
    }

    /**
     * Stores the contents of a ProducerRecord in the MockKafkaDatastore
     * @param record the producer record to be stored in the MockKafkaDatastore
     */
    synchronized void send(ProducerRecord<K, V> record) {
        if (store.containsKey(record.topic())) {
            store.get(record.topic()).offer(new ImmutablePair<>(record.key(), record.value()));
        } else {
            LinkedBlockingQueue<Pair<K, V>> queue = new LinkedBlockingQueue<>();
            queue.offer(new ImmutablePair<>(record.key(), record.value()));
            store.put(record.topic(), queue);
        }
        logger.trace("Message saved to Kafka: topic = {}, message = {}",
                v("messaging_topic", record.topic()),
                v("messaging_message", record.value()));
    }

    /**
     * Retrieves data from the MockKafkaDatastore from the specified topics as ConsumerRecords
     * @param topics The topics from which to poll data
     * @return The retrieved ConsumerRecords
     */
    synchronized ConsumerRecords<K, V> poll(Collection<String> topics) {
        HashMap<TopicPartition, List<ConsumerRecord<K, V>>> map = new HashMap<>();
        for (String topic : topics) {
            LinkedList<Pair<K, V>> pairs = new LinkedList<>();
            if (store.containsKey(topic)) {
                store.get(topic).drainTo(pairs);
            }
            LinkedList<ConsumerRecord<K, V>> list = new LinkedList<>();
            for(Pair<K, V> pair : pairs) {
                ConsumerRecord<K, V> record = new ConsumerRecord<K, V>(topic, 0, 0, pair.getKey(), pair.getValue());
                list.add(record);
            }
            if (list.size() > 0) {
                map.put(new TopicPartition(topic, 0), list);
            }
        }
        ConsumerRecords<K, V> records = new ConsumerRecords<>(map);
        if (records.count() > 0) {
            logger.trace("Retrieved records from Kafka");
        }

        return records;
    }

    /**
     * Retrieves data from the MockKafkaDatastore from the specified topics as ConsumerRecords
     * @param topicPattern The regex pattern that specifies topic matches from which to poll data
     * @return The retrieved ConsumerRecords
     */
    synchronized ConsumerRecords<K, V> poll(Pattern topicPattern) {
        HashMap<TopicPartition, List<ConsumerRecord<K, V>>> map = new HashMap<>();
        LinkedList<Pair<K, V>> pairs = new LinkedList<>();
        for (HashMap.Entry<String, LinkedBlockingQueue<Pair<K, V>>> entry : store.entrySet()) {
            if (topicPattern.matcher(entry.getKey()).matches())
                entry.getValue().drainTo(pairs);
            LinkedList<ConsumerRecord<K, V>> list = new LinkedList<>();
            for(Pair<K, V> pair : pairs) {
                ConsumerRecord<K, V> record = new ConsumerRecord<K, V>(entry.getKey(), 0, 0, pair.getKey(), pair.getValue());
                list.add(record);
            }
            if (list.size() > 0) {
                map.put(new TopicPartition(entry.getKey(), 0), list);
            }
        }

        ConsumerRecords<K, V> records = new ConsumerRecords<>(map);
        if (records.count() > 0) {
            logger.debug("Retrieved records from Kafka");
        }

        return records;
    }
}
