package dk.s4.microservices.messaging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;
import org.apache.commons.lang3.Validate;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.List;

import static net.logstash.logback.argument.StructuredArguments.v;

public class EventConsumer {
    private final static Logger logger = LoggerFactory.getLogger(EventConsumer.class);

    private final List<IEventConsumerInterceptor> interceptors = new ArrayList<>();

    protected EventProcessor eventProcessor;
    protected EventProducer eventProducer;

    protected String topicsString = "";
    protected Pattern topicPattern = null;

    public EventConsumer() {};

    public EventConsumer(List<Topic> topics,
                         EventProducer eventProducer,
                         EventProcessor eventProcessor)
            throws MessagingInitializationException {

        if (topics.isEmpty())
            throw new MessagingInitializationException("EventConsumer constructor: Topic list is empty");

        this.eventProducer = eventProducer;
        if (eventProducer == null)
            logger.warn("EventConsumer initialized with EventProducer = null");

        this.eventProcessor = eventProcessor;
        if (eventProcessor == null)
            throw new MessagingInitializationException("EventConsumer constructor: eventProcessor is null");

        for(Topic t : topics)
            topicsString += t.toString() + ", ";
        logger.info("EventConsumer subscribing to: {}", topicsString);
    }

    public EventConsumer(java.util.regex.Pattern topicPattern,
            EventProducer eventProducer,
            EventProcessor eventProcessor)
            throws MessagingInitializationException {

        if (topicPattern.pattern().isEmpty())
            throw new MessagingInitializationException("EventConsumer constructor: Topic pattern is empty");

        this.eventProducer = eventProducer;
        if (eventProducer == null)
            logger.warn("EventConsumer initialized with EventProducer = null");

        this.eventProcessor = eventProcessor;
        if (eventProcessor == null)
            throw new MessagingInitializationException("EventConsumer constructor: eventProcessor is null");

        logger.info("EventConsumer subscribing to: {}", topicPattern.pattern());
    }

    /**
     * Returns a list of all registered consumer interceptors
     *
     * @return list of all registered consumer interceptors
     */
    public List<IEventConsumerInterceptor> getInterceptors() {
        return Collections.unmodifiableList(interceptors);
    }

    /**
     * Sets (or clears) the list of interceptors
     *
     * @param theInterceptors The list of interceptors (may be null)
     */
    public void setInterceptors(IEventConsumerInterceptor... theInterceptors) {
        Validate.noNullElements(theInterceptors, "theInterceptors must not contain any null elements");

        interceptors.clear();
        if (theInterceptors != null) {
            interceptors.addAll(Arrays.asList(theInterceptors));
        }
    }

    /**
     * Sets (or clears) the list of interceptors
     *
     * @param theList The list of interceptors (may be null)
     */
    public void setInterceptors(List<IEventConsumerInterceptor> theList) {
        interceptors.clear();
        if (theList != null) {
            interceptors.addAll(theList);
        }
    }

    /**
     * Adds interceptor to list of interceptors if it's not already in list.
     *
     * @param theInterceptor Interceptor to register
     */
    public void registerInterceptor(IEventConsumerInterceptor theInterceptor) {
        if (!interceptors.contains(theInterceptor)) {
            interceptors.add(theInterceptor);
        }
    }


    protected boolean doProcessMessage(String topic, String msg) {
         //Notify interceptors about incoming message
        for (IEventConsumerInterceptor next : interceptors) {
            boolean continueProcessing = next.incomingMessagePreParsing(topic, msg);
            if (!continueProcessing) {
                logger.trace("Interceptor {} returned false, not continuing processing", next);
                return false;
            }
        }

        try {
            Message message = MessageParser.parse(msg);
            Topic consumedTopic = TopicParser.parse(topic);

            MDC.put("correlationId", MessagingUtils.verifyOrCreateId(message.getCorrelationId()));
            MDC.put("transactionId", message.getTransactionId());

            logger.trace("EventConsumer processing: topic = {}", v("messaging_topic", topic));

            //Notify interceptors about incoming message after parsing of topic and message
            for (IEventConsumerInterceptor next : interceptors) {
                boolean continueProcessing = next.incomingMessagePreProcessMessage(consumedTopic, message);
                if (!continueProcessing) {
                    logger.trace("Interceptor {} returned false, not continuing processing", next);
                    return false;
                }
            }

            Topic messageProcessedTopic = new Topic();
            Message outgoingMessage = new Message();

            eventProcessor.processMessage(consumedTopic, message, messageProcessedTopic, outgoingMessage);

            //Notify interceptors about incoming message after processing, but before sending response
            for (IEventConsumerInterceptor next : interceptors) {
                boolean continueProcessing = next.incomingMessagePostProcessMessage(messageProcessedTopic, message);
                if (!continueProcessing) {
                    logger.trace("Interceptor {} returned false, not continuing processing", next);
                    return false;
                }
            }

            sendMessageProcessedEvent(messageProcessedTopic, message, outgoingMessage);

            //Notify interceptors that processing of incoming message completed normally
            for (IEventConsumerInterceptor next : interceptors) {
                next.processingCompletedNormally(messageProcessedTopic, message);
            }
        } catch (Exception e) {
            handleException(topic, msg, e);
            return false;
        }

        return true;
    }

    protected void handleException(String topic, String msg, Exception e) {
        //Notify interceptors about incoming message after processing, but before sending response
        for (IEventConsumerInterceptor next : interceptors) {
            boolean continueProcessing = next.handleException(topic, msg, e);
            if (!continueProcessing) {
                logger.trace("Interceptor {} returned false, not continuing processing", next);
                return ;
            }
        }

        String errorMsg = "";
        if (e instanceof ParseException)
            errorMsg = "Message received is not valid JSON";
        else if (e instanceof  MessageParseException)
            errorMsg = "Can not parse message as a 4S Message";
        else if (e instanceof TopicParseException)
            errorMsg = "Can not parse topic as a 4S Topic";
        else if (e instanceof MessageIncompatibleException)
            errorMsg = "Message version of received message is not compatible with current message version";
        else
            errorMsg = "An error occured";

        logger.error(errorMsg + ": " + e.getMessage(), e);
        Topic messageProcessedTopic = new Topic().setOperation(Topic.Operation.ProcessingFailed);
        sendMessageProcessedEvent(messageProcessedTopic,
                new EventOperationOutcome(EventOperationOutcome.ERROR, errorMsg).toString());
    }

    private void sendMessageProcessedEvent(Topic messageProcessedTopic, String operationOutcome) {
        Message outgoingMessage = new Message()
                .setBody(operationOutcome)
                .setBodyCategory(Message.BodyCategory.FHIR)
                .setBodyType("OperationOutcome")
                .setContentVersion(System.getenv("FHIR_VERSION"));
        sendMessageProcessedEvent(messageProcessedTopic, new Message(), outgoingMessage);
    }

    private void sendMessageProcessedEvent(Topic messageProcessedTopic,
                                           Message consumedMessage,
                                           Message outgoingMessage) {
        if (eventProducer == null)
            logger.warn("sendMessageProcessedEvent: Can not send event - EventConsumer has eventProducer = null");
        else {
            if (outgoingMessage.getCorrelationId() == null || outgoingMessage.getCorrelationId().isEmpty())
                outgoingMessage.setCorrelationId(consumedMessage.getCorrelationId());
            if (outgoingMessage.getTransactionId() == null || outgoingMessage.getTransactionId().isEmpty())
                outgoingMessage.setTransactionId(consumedMessage.getTransactionId());

            eventProducer.sendMessage(messageProcessedTopic, outgoingMessage);
        }
    }
}