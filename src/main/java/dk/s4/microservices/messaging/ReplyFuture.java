package dk.s4.microservices.messaging;

import dk.s4.microservices.messaging.Message;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ReplyFuture implements Future<Message> {
    private final CountDownLatch latch = new CountDownLatch(1);
    private Message value;

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return latch.getCount() == 0;
    }

    @Override
    public Message get() throws InterruptedException {
        latch.await();
        return value;
    }

    @Override
    public Message get(long timeout, TimeUnit unit) throws InterruptedException, TimeoutException {
        if (latch.await(timeout, unit)) {
            return value;
        } else {
            throw new TimeoutException();
        }
    }

    // calling this more than once doesn't make sense, and won't work properly in this implementation. so: don't.
    public void put(Message result) {
        value = result;
        latch.countDown();
    }
}