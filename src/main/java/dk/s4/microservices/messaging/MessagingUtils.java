package dk.s4.microservices.messaging;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Random;

public class MessagingUtils {

	private final static Logger logger = LoggerFactory
			.getLogger(MessagingUtils.class);

	private static Random random = new Random();
    private static final int MAX_ID_SIZE = 50;

	public static String getEnv(String env, String defaultValue) {
		try {
			String value = System.getenv(env);
			if (value != null) {
				return value;
			} else {
				return defaultValue;
			}
		} catch (Exception perm) {
			logger.error("Error loading environment variable: " + perm);
		}
		return defaultValue;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////
    // All correlationId code below taken from Developer's Garden blog:
    // http://www.dev-garden.org/2015/11/28/a-recipe-for-microservice-correlation-ids-in-java-servlets/

    public static String verifyOrCreateId(String correlationId)
    {
        if(correlationId == null)
        {
            correlationId = generateCorrelationId();
        }
        //prevent on-purpose or accidental DOS attack that
        // fills logs with long correlation id provided by client
        else if (correlationId.length() > MAX_ID_SIZE)
        {
            correlationId = correlationId.substring(0, MAX_ID_SIZE);
        }

        return correlationId;

    }

    private static String generateCorrelationId()
    {
        long randomNum = random.nextLong();
        return encodeBase62(randomNum);

    }

    /**
     * Encode the given Long in base 62
     * @param n  Number to encode
     * @return Long encoded as base 62
     */
    private static String encodeBase62(long n)
    {

        final String base62Chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder builder = new StringBuilder();

        //NOTE:  Appending builds a reverse encoded string.  The most significant value
        //is at the end of the string.  You could prepend(insert) but appending
        // is slightly better performance and order doesn't matter here.

        //perform the first selection using unsigned ops to get negative
        //numbers down into positive signed range.
        long index = Long.remainderUnsigned(n, 62);
        builder.append(base62Chars.charAt((int) index));
        n = Long.divideUnsigned(n, 62);
        //now the long is unsigned, can just do regular math ops
        while (n > 0)
        {
            builder.append(base62Chars.charAt((int) (n % 62)));
            n /= 62;
        }
        return builder.toString();
    }
}
