package dk.s4.microservices.messaging;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Class for producing messages on the form:
 *
 * {
 *  "header":
 *  {
 *      "sender": "obs-input-service",
 *      "bodyCategory": "FHIR",
 *      "bodyType": "Observation",
 *      "contentVersion": "3.3.0",
 *      "location":"http://fhirtest.uhn.ca/baseDstu3/Observation/15354/_history/1"
 *      "correlationId": "xez5ZXQcDG6"
 *      "transactionId": "e6651fe2-fb8e-4a54-8b8e-7343dbdb997c"
 *      "security": ...
 *  },
 *  "body":
 *  {
 *       "resourceType": "Observation",
 *        ...
 *   }
 * }
 *
 */
public class Message {

    private final static Logger logger = LoggerFactory.getLogger(Message.class);

    //Change this whenever you make changes to the Message format! Uses semantic versioning
    private final Version VERSION = new Version(3, 0, 0);

    //Headers
    private Version headerVersion;
    private String sender;
    private BodyCategory bodyCategory;
    private String bodyType;
    private String contentVersion;
    private Prefer prefer;
    private String etag;
    private String ifMatch;
    private String ifNoneExist;
    private String location;
    private String correlationId;
    private String transactionId;
    private String security;
    private String session;

    //Body
    private String body;

    public Message() { setHeaderVersion(getVersion()); }

    /**
     * Gets the Message version in the messaging library
     *
     * @return version String
     */
    public Version getVersion() {
        return VERSION;
    }

    public enum BodyCategory {
        FHIR, CDA, System
    }

    /**
     * Represents values for the "return" value as described at http://hl7.org/fhir/2018Sep/http.html#ops
     */
    public enum Prefer {
        REPRESENTATION, OPERATIONOUTCOME, MINIMAL
    }

    /**
     * Values of fields of input message will be assigned to the fields of this Message.
     * Notice: etags, ifMatch, ifNoneExist and location will not be cloned!
     *
     * @param other Message whos fields this method is to clone
     *
     * @throws MessageIncompatibleException in case of message header version mismatch
     */
    public void cloneFields(Message other) throws MessageIncompatibleException {
        if (!this.getHeaderVersion().isCompatible(other.getHeaderVersion()))
            throw new MessageIncompatibleException("Message versions are incompatible. This version = "
                    + this.getHeaderVersion() + " other version = " + other.getHeaderVersion());
        this.setSender(other.getSender());
        this.setBodyCategory(other.getBodyCategory());
        this.setBodyType(other.getBodyType());
        this.setContentVersion(other.getContentVersion());
        this.setPrefer(other.getPrefer());

        this.setLocation(other.getLocation());
        this.setCorrelationId(other.getCorrelationId());
        this.setTransactionId(other.getTransactionId());
        this.setSecurity(other.getSecurity());
        this.setSession(other.getSession());
        this.setBody(other.getBody());
    }

    /**
     * Gets the version in the header of the Message
     *
     * @return Version
     */
    public Version getHeaderVersion() {
        return headerVersion;
    }

    /**
     * Sets the version in the header of the Message
     *
     * @param version
     * @return this Message
     */
    public Message setHeaderVersion(Version version) {
        this.headerVersion = version;
        return this;
    }

    public String getSender() {
        return sender;
    }

    public Message setSender(String sender) {
        this.sender = sender;
        return this;
    }

    public BodyCategory getBodyCategory() {
        return bodyCategory;
    }

    public Message setBodyCategory(BodyCategory bodyCategory) {
        this.bodyCategory = bodyCategory;
        return this;
    }

    /**
     * Parses a String into a {@link Message.BodyCategory} value
     *
     * @param catString {@link Message.BodyCategory} as String
     * @return {@link Message.BodyCategory} value matching the input String. If no match is found null is returned.
     */
    public static Message.BodyCategory bodyCategoryFromString(String catString) {
        if (Message.BodyCategory.FHIR.name().equals(catString))
            return Message.BodyCategory.FHIR;
        if (Message.BodyCategory.CDA.name().equals(catString))
            return Message.BodyCategory.CDA;
        if (Message.BodyCategory.System.name().equals(catString))
            return Message.BodyCategory.System;
        else
            return null;
    }

    public String getBodyType() {
        return bodyType;
    }

    public Message setBodyType(String bodyType) {
        this.bodyType = bodyType;
        return this;
    }

    public String getContentVersion() {
        return contentVersion;
    }

    public Message setContentVersion(String contentVersion) {
        this.contentVersion = contentVersion;
        return this;
    }
    
    public Prefer getPrefer() {
        return prefer;
    }

    public Message setPrefer(Prefer prefer) {
        this.prefer = prefer;
        return this;
    }

    public String getEtag() {
        return etag;
    }

    public Message setEtag(String etag) {
        this.etag = etag;
        return this;
    }

    /**
     * Gets the version part of an Etag.
     *
     * E.g. if Etag contains a weak etag {@literal W/"4232"} this function will return the string 4232.
     *
     * @return String containing version part of an Etag. Returns null ff Etag is null.
     */
    public String getEtagVersion() {
        return extractEtagTypeVersion(etag);
    }

    /**
     * Sets the version formatted as a weak Etag
     *
     * @param version
     * @return this Message
     */
    public Message setWeakEtagVersion(String version) {
        if (StringUtils.isNotBlank(version)) {
            version = StringUtils.prependIfMissing(version, "\"");
            version = StringUtils.appendIfMissing(version, "\"");
            etag = "W/" + version;
        }
        return this;
    }

    public String getIfMatch() {
        return ifMatch;
    }

    public Message setIfMatch(String ifMatch) {
        this.ifMatch = ifMatch;
        return this;
    }

    public String getIfNoneExist() {
        return ifNoneExist;
    }

    public Message setIfNoneExist(String ifNoneExist) {
        this.ifNoneExist = ifNoneExist;
        return this;
    }

    /**
     * Gets the version part of an If-Match header.
     *
     * E.g. if If-Match contains a weak etag {@literal W/"4232"} this function will return the string 4232.
     *
     * @return String containing version part of an If-Match header. Returns null if ifMatch is null.
     */
    public String getIfMatchVersion() {
        return extractEtagTypeVersion(ifMatch);
    }

    public Message SetWeakIfMatchVersion(String version) {
        if (StringUtils.isNotBlank(version)) {
            version = StringUtils.prependIfMissing(version, "\"");
            version = StringUtils.appendIfMissing(version, "\"");
            ifMatch = "W/" + version;
        }
        return this;
    }

    /**
     * Parses a String into a {@link Message.Prefer} value
     *
     * @param preferString {@link Message.Prefer} as String
     * @return {@link Message.Prefer} value matching the input String. If no match is found null is returned.
     */
    public static Message.Prefer preferFromString(String preferString) {
        if (preferString.isEmpty())
            return null;
        if (!preferString.contains("="))
            return null;

        String[] splitStr = preferString.split("=");
        if (splitStr.length != 2)
            return null;

        if (splitStr[0].equalsIgnoreCase("return")) {
            if (Prefer.REPRESENTATION.name().equalsIgnoreCase(splitStr[1]))
                return Prefer.REPRESENTATION;
            if (Prefer.OPERATIONOUTCOME.name().equalsIgnoreCase(splitStr[1]))
                return Prefer.OPERATIONOUTCOME;
            if (Prefer.MINIMAL.name().equalsIgnoreCase(splitStr[1]))
                return Prefer.MINIMAL;
            else
                return null;
        }
        else
            return null;
    }

    public String getLocation() {
        return location;
    }

    public Message setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public Message setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
        return this;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Message setTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public String getSecurity() {
        return security;
    }

    public Message setSecurity(String security) {
        this.security = security;
        return this;
    }

    public String getSession() {
        return session;
    }

    public Message setSession(String session) {
        this.session = session;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Message setBody(String body) {
        this.body = body;
        return this;
    }

    public String toString() {
        JSONObject jsonObject = new JSONObject();
        JSONObject header = new JSONObject();
        jsonObject.put("header", header);
        if (getHeaderVersion() != null) header.put("version", getHeaderVersion().toString());
        if (getSender() != null) header.put("sender", getSender());
        if (getBodyCategory() != null) header.put("bodyCategory", getBodyCategory().name());
        if (getBodyType() != null) header.put("bodyType", getBodyType());
        if (getContentVersion() != null) header.put("contentVersion", getContentVersion());
        if (getPrefer() != null) header.put("prefer", "return=" + getPrefer().name().toLowerCase());
        if (getEtag() != null) header.put("etag", getEtag());
        if (getIfMatch() != null) header.put("ifMatch", getIfMatch());
        if (getIfNoneExist() != null) header.put("ifNoneExist", getIfNoneExist());
        if (getLocation() != null) header.put("location", getLocation());
        if (getCorrelationId() != null) header.put("correlationId", getCorrelationId());
        if (getTransactionId() != null) header.put("transactionId", getTransactionId());
        if (getSecurity() != null) header.put("security", getSecurity());
        if (getSession() != null) header.put("session", getSession());

        if (getBody() != null) jsonObject.put("body", getBody());
        else logger.warn("KafkaMessage.toString: no body");

        return jsonObject.toString();
    }

    private String extractEtagTypeVersion(String versionString) {
        if (versionString == null)
            return null;

        //Use regexp matcher to extract version:
        Pattern p = Pattern.compile("W/\"(.*)\"");
        Matcher m = p.matcher(versionString);
        if (!m.matches())
            return versionString;
        return m.group(1);
    }
}
