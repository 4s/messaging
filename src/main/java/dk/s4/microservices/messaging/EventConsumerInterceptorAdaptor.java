package dk.s4.microservices.messaging;

/**
 * Base class for {@link IEventConsumerInterceptor} implementations. Provides a No-op implementation
 * of all methods, always returning <code>true</code>.
 *
 * <p>
 * This class is heavily inspired by HAPI FHIR server interceptors (<a href="http://jamesagnew.github.io/hapi-fhir/doc_rest_server_interceptor.html">HAPI FHIR server interceptor documentation</a>).
 * </p>
 */

public class EventConsumerInterceptorAdaptor implements IEventConsumerInterceptor {

    @Override
    public boolean incomingMessagePreParsing(String consumedTopic, String receivedMessage) {
        return true;
    }

    @Override
    public boolean incomingMessagePreProcessMessage(Topic consumedTopic, Message receivedMessage) {
        return true;
    }

    @Override
    public boolean incomingMessagePostProcessMessage(Topic consumedTopic, Message receivedMessage) {
        return true;
    }

    @Override
    public boolean handleException(String consumedTopic, String receivedMessage, Exception exception) {
        return true;
    }

    @Override
    public void processingCompletedNormally(Topic consumedTopic, Message receivedMessage) { }
}
