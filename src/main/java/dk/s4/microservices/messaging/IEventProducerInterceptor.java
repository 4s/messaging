package dk.s4.microservices.messaging;

import java.io.IOException;

/**
 * Provides methods to intercept messages produced by instances of {@link dk.s4.microservices.messaging.EventProducer}
 * and potentially change or take action on messages before they are sent.
 *
 *
 * <p>
 * This class is heavily inspired by HAPI FHIR server interceptors
 * (<a href="http://jamesagnew.github.io/hapi-fhir/doc_rest_client_interceptor.html">HAPI Documentation Client Interceptor</a>).
 * </p>
 */

public interface IEventProducerInterceptor {

    /**
     * Invoked by the EventProducer just before sending the message. May modify message sent or prevent sending
     * all together.
     *
     * @param topic that message is about to be sent to. May be modified by {@code interceptSend}.
     * @param message message that is about to be sent. May be modified by {@code interceptSend}.
     * @return Return <code>true</code> if sending of message should proceed. Return <code>false</code> if you want to
     * prevent the message from being sent. In this case no further interceptors will be called.
     */
    boolean interceptSend(Topic topic, Message message);

    /**
     *
     */
    /**
     * Invoked by the EventProducer just after completing sending the message.
     *
     * @param topic topic that message was sent to.
     * @param message message that was sent.
     */
    void interceptSent(Topic topic, Message message);

}
