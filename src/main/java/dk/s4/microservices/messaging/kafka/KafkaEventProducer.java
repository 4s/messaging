package dk.s4.microservices.messaging.kafka;

import dk.s4.microservices.messaging.EventProducer;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.AuthenticationException;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.errors.TimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Map;
import java.util.Properties;

/**
 * The KafkaEventProducer writes messages to a topic.
 */
public class KafkaEventProducer extends EventProducer {

    private final Logger logger = LoggerFactory.getLogger(KafkaEventProducer.class);

    private KafkaProducer<String, String> kafkaProducer;

    /**
     * Constructor loads properties from environment and instantiates internal KafkaProducer.
     *
     * @param sender The sender of messages.
     * @throws KafkaInitializationException
     */
    public KafkaEventProducer(String sender) throws KafkaInitializationException {
        super(sender);
        kafkaProducer = new KafkaProducer<>(loadProducerPropertiesFromEnv());
    }

    /**
     * Sends message to Kafka on given topic.
     *
     * @param topic   Destination Kafka topic.
     * @param message Message to send to Kafka.
     * @throws AuthenticationException
     * @throws IllegalStateException
     * @throws SerializationException
     * @throws TimeoutException
     * @throws KafkaException
     */
    protected void doSendMessage(Topic topic, Message message) throws AuthenticationException, IllegalStateException, SerializationException, TimeoutException, KafkaException {
        @SuppressWarnings("unchecked")
        Map<String, String> mdcContextMap = MDC.getCopyOfContextMap();
        message.setSender(sender);
        ProducerRecord<String, String> record = new ProducerRecord<>(topic.toString(), message.toString());
        kafkaProducer.send(record, (metadata, e) -> {
            if (mdcContextMap != null)
                MDC.setContextMap(mdcContextMap); //ensure callback thread has same MDC context
            else
                logger.warn("MDC context map is null");
            if (e != null) {
                logger.error("Error during sendMessageToKafka: " + e.getMessage(), e);
                e.printStackTrace();
            } else {
                logger.trace("sendMessageSuccess: Message sent to topic = " + metadata.topic() +
                        ", partition = " + metadata.partition() +
                        ", offset = " + metadata.offset());
            }
        });
    }

    private Properties loadProducerPropertiesFromEnv() throws KafkaInitializationException {
        //Get KafkaProducer configuration from environment
        Properties properties = new Properties();
        String kafka_bootstrap_server = System.getenv("KAFKA_BOOTSTRAP_SERVER");
        if (kafka_bootstrap_server != null) {
            properties.put("bootstrap.servers", kafka_bootstrap_server);

            String kafka_key_serializer = System.getenv("KAFKA_KEY_SERIALIZER");
            String kafka_value_serializer = System.getenv("KAFKA_VALUE_SERIALIZER");
            if (kafka_key_serializer != null && kafka_value_serializer != null) {
                properties.put("key.serializer", kafka_key_serializer);
                properties.put("value.serializer", kafka_value_serializer);

                String kafka_acks = System.getenv("KAFKA_ACKS");
                if (kafka_acks == null)
                    logger.warn("KafkaProducer initialization: KAFKA_ACKS not specified (Kafka defaults to acks=1)");
                else
                    properties.put("acks", kafka_acks);
                String kafka_retries = System.getenv("KAFKA_RETRIES");
                if (kafka_retries == null)
                    logger.warn("KafkaProducer initialization: KAFKA_RETRIES not specified (Kafka defaults to retries=0)");
                else
                    properties.put("retries", kafka_retries);
                String kafka_max_request_size = System.getenv("KAFKA_MAX_REQUEST_SIZE");
                if (kafka_max_request_size  == null)
                    logger.warn("KafkaProducer initialization: KAFKA_MAX_REQUEST_SIZE not specified (Kafka defaults to max_request_size=1MB)");
                else
                    properties.put("max.request.size", kafka_max_request_size );

            } else
                throw new KafkaInitializationException("KafkaProducer not intialized: Either KAFKA_KEY_SERIALIZER or KAFKA_VALUE_SERIALIZER (or both) is undefined!");
        } else
            throw new KafkaInitializationException("KafkaProducer not intialized: KAFKA_BOOTSTRAP_SERVER is undefined!");

        return properties;
    }
}
