package dk.s4.microservices.messaging.kafka;

import dk.s4.microservices.messaging.EventConsumer;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.consumer.InvalidOffsetException;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static net.logstash.logback.argument.StructuredArguments.v;

/**
 * Class for consuming, processing and responding to messages over Kafka.
 */
public class KafkaConsumeAndProcess extends EventConsumer implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(KafkaConsumeAndProcess.class);

    private volatile boolean running;

    private boolean enableHealth = false;
    private long lastHealthTimeNano = 0;
    private int healthIntervalMs = 5000;
    private static String healthFilePath = "/tmp/health";
    private File healthFile;

    protected KafkaConsumer<String, String> kafkaConsumer;

    /**
     * Constructor loads properties from environment and instantiates internal KafkaConsumer.
     *
     * @param topics Kafka topics to subscribe to.
     * @param eventProducer KafkaEventProducer used for responding to messages (result and/or status of processing)
     * @param eventProcessor Instance of implementation of {@link dk.s4.microservices.messaging.EventProcessor}. The
     *                       EventProcessor implementation implements the main processing invoked on reception of
     *                       message.
     * @throws KafkaInitializationException
     * @throws MessagingInitializationException
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     */
    public KafkaConsumeAndProcess(List<Topic> topics,
                                  KafkaEventProducer eventProducer,
                                  EventProcessor eventProcessor)
            throws KafkaInitializationException, MessagingInitializationException, IllegalArgumentException,
            IllegalStateException, KafkaException {
        super(topics, eventProducer, eventProcessor);

        kafkaConsumer = new KafkaConsumer<String, String>(loadConsumerPropertiesFromEnv());

        List<String> topicStrings = new ArrayList<>();
        for (Topic topic: topics)
            topicStrings.add(topic.toString());
        kafkaConsumer.subscribe(topicStrings);
    }

    /**
     * Constructor loads properties from environment and instantiates internal KafkaConsumer.
     *
     * @param topicPattern Subscribe to all Kafka topics matching specified pattern.
     * @param eventProducer KafkaEventProducer used for responding to messages (result and/or status of processing)
     * @param eventProcessor Instance of implementation of {@link dk.s4.microservices.messaging.EventProcessor}. The
     *                       EventProcessor implementation implements the main processing invoked on reception of
     *                       message.
     * @throws KafkaInitializationException
     * @throws MessagingInitializationException
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     */
    public KafkaConsumeAndProcess(java.util.regex.Pattern topicPattern,
            KafkaEventProducer eventProducer,
            EventProcessor eventProcessor)
            throws KafkaInitializationException, MessagingInitializationException, IllegalArgumentException,
            IllegalStateException, KafkaException {
        super(topicPattern, eventProducer, eventProcessor);

        kafkaConsumer = new KafkaConsumer<String, String>(loadConsumerPropertiesFromEnv());

        kafkaConsumer.subscribe(topicPattern);
    }

    @Override
    public void run() {
        logger.info("Starting KafkaConsumeAndProcessThread");
        running = true;
        while (running) {
            try {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(100);
                healthy();
                for (ConsumerRecord<String, String> record : records) {
                    healthy();
                    logger.trace("KafkaConsumeAndProcess processing: topic = {} offset = {}",
                            v("messaging_topic", record.topic()),
                            v("kafka_offset", record.offset()));

                    try {
                        doProcessMessage(record.topic(), record.value());
                    } catch (AuthenticationException | IllegalStateException
                            | SerializationException | TimeoutException e) {
                        logger.error("Error sending message to Kafka: "+ e.getMessage(), e);
                    } catch (KafkaException e) {
                        logger.error("Error sending message to Kafka: "+ e.getMessage(), e);
                    }

                    try {
                        kafkaConsumer.commitSync(); //Commit offset after write (or not) to db and after telling the world about it
                    } catch (CommitFailedException | WakeupException | AuthenticationException
                            | AuthorizationException e) {
                        logger.error("Error commiting offset to Kafka: "+ e.getMessage(), e);
                    } catch (KafkaException e) {
                        logger.error("Error commiting offset to Kafka: "+ e.getMessage(), e);
                    }

                    MDC.clear();
                }
            } catch (InvalidOffsetException | WakeupException | AuthenticationException
                    | AuthorizationException | IllegalArgumentException | IllegalStateException e) {
                logger.error("Error polling Kafka topics {}: {}", topicsString, e.getMessage());
                logger.error("Exception polling Kafka topics"+ e.getMessage(), e);
            } catch (KafkaException e) {
                logger.error("Error polling Kafka topics {}: {}", topicsString, e.getMessage());
                logger.error("Exception polling Kafka topics"+ e.getMessage(), e);
            }
        }
        logger.debug("Closing kafkaConsumer");
        kafkaConsumer.close();
    }

    public void stopThread() {
        running = false;
    }

    private void healthy() {
        if (enableHealth) {
            try {
                if (lastHealthTimeNano == 0 || healthFile == null) {
                    healthFile = new File(healthFilePath);
                    touch(healthFile);
                    lastHealthTimeNano = System.nanoTime();
                } else {
                    long timeNowNano = System.nanoTime();
                    if (TimeUnit.NANOSECONDS.toMillis(timeNowNano - lastHealthTimeNano) > healthIntervalMs) {
                        touch(healthFile);
                        lastHealthTimeNano = System.nanoTime();
                    }
                }
            } catch (IOException e) {
                logger.error("Failed touching health file at " + healthFilePath + ": " + e.getMessage(), e);
            }
        }
    }

    private void touch(File file) throws IOException {
        long timestamp = System.currentTimeMillis();
        touch(file, timestamp);
    }

    /**
     * Returns true if the KafkaConsumeAndProcessThread is healthy - i.e. it runs and checks for new consumer records.
     * At most 2*healthIntervalMs milliseconds between checks for the thread to be deemed healthy.
     *
     * @return true if the KafkaConsumeAndProcessThread is healthy.
     */
    public boolean isHealthy() {
        long timeNowNano = System.nanoTime();
        if (TimeUnit.NANOSECONDS.toMillis(timeNowNano - lastHealthTimeNano) > 2*healthIntervalMs) {
            return false;
        }
        return true;
    }

    public void touch(File file, long timestamp) throws IOException{
        if (!file.exists()) {
            new FileOutputStream(file).close();
        }

        file.setLastModified(timestamp);
    }

    private Properties loadConsumerPropertiesFromEnv() throws KafkaInitializationException {
        //Get KafkaConsumer configuration from environment
        Properties properties = new Properties();
        String kafka_bootstrap_server = System.getenv("KAFKA_BOOTSTRAP_SERVER");
        if (kafka_bootstrap_server != null) {
            properties.put("bootstrap.servers", kafka_bootstrap_server);

            //Health
            String enableHealthString = System.getenv("ENABLE_HEALTH");
            if (enableHealthString != null && !enableHealthString.isEmpty()
                    && enableHealthString.equalsIgnoreCase("true")) {

                enableHealth = true;

                healthFilePath = System.getenv("HEALTH_FILE_PATH");
                if (healthFilePath == null || healthFilePath.isEmpty())
                    throw new KafkaInitializationException("Health is enabled but HEALTH_FILE_PATH is undefined");

                String healthIntervalMsString = System.getenv("HEALTH_INTERVAL_MS");
                if (healthIntervalMsString == null || healthIntervalMsString.isEmpty())
                    throw new KafkaInitializationException("Health is enabled but HEALTH_INTERVAL_MS is undefined");
                else
                    healthIntervalMs = Integer.parseInt(healthIntervalMsString);
            }

            String kafka_key_deserializer = System.getenv("KAFKA_KEY_DESERIALIZER");
            String kafka_value_deserializer = System.getenv("KAFKA_VALUE_DESERIALIZER");
            if (kafka_key_deserializer != null && kafka_value_deserializer != null) {
                properties.put("key.deserializer", kafka_key_deserializer);
                properties.put("value.deserializer", kafka_value_deserializer);

                String kafka_group_id = System.getenv("KAFKA_GROUP_ID");
                if (kafka_group_id == null)
                    logger.warn("KafkaConsumer initialization: KAFKA_GROUP_ID not specified");
                else
                    properties.put("group.id", kafka_group_id);

                String kafka_enable_auto_commit = System.getenv("KAFKA_ENABLE_AUTO_COMMIT");
                if (kafka_enable_auto_commit == null)
                    logger.warn("KafkaConsumer initialization: KAFKA_ENABLE_AUTO_COMMIT not specified (Kafka defaults to enable.auto.commit=true");
                else
                    properties.put("enable.auto.commit", kafka_enable_auto_commit);

                String kafka_auto_commit_interval_ms = System.getenv("KAFKA_AUTO_COMMIT_INTERVAL_MS");
                if (kafka_auto_commit_interval_ms == null)
                    logger.warn("KafkaConsumer initialization: KAFKA_AUTO_COMMIT_INTERVAL_MS not specified (Kafka defaults to auto.commit.interval.ms=5000");
                else
                    properties.put("auto.commit.interval.ms", kafka_auto_commit_interval_ms);

                String kafka_session_timeout_ms = System.getenv("KAFKA_SESSION_TIMEOUT_MS");
                if (kafka_session_timeout_ms == null)
                    logger.warn("KafkaConsumer initialization: KAFKA_SESSION_TIMEOUT_MS not specified (Kafka defaults to session.timeout.ms=10000");
                else
                    properties.put("session.timeout.ms", kafka_session_timeout_ms);

                properties.put("metadata.max.age.ms", 5000);
                properties.put("auto.offset.reset", "earliest");
            }
            else
                throw new KafkaInitializationException("KafkaConsumer properties: Either KAFKA_KEY_DESERIALIZER or KAFKA_VALUE_DESERIALIZER (or both) is undefined!");
        }
        else
            throw new KafkaInitializationException("KafkaConsumer properties: KAFKA_BOOTSTRAP_SERVER is undefined!");

        return properties;
    }
}
