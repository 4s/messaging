package dk.s4.microservices.messaging.kafka;

public class KafkaInitializationException extends Exception {
    public KafkaInitializationException() {
    }

    public KafkaInitializationException(String message) {
        super(message);
    }

    public KafkaInitializationException(Throwable cause) {
        super(cause);
    }

    public KafkaInitializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
