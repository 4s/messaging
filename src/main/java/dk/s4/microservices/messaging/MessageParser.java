package dk.s4.microservices.messaging;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageParser {
    private final static Logger logger = LoggerFactory.getLogger(MessageParser.class);

    /**
     * Will parse a message on the form below and return a corresponding Message object:
     *
     * {
     *  "header":
     *  {
     *      "sender": "obs-input-service",
     *      "bodyCategory": "FHIR",
     *      "bodyType": "Observation",
     *      "contentVersion": "3.3.0",
     *      "prefer": "return=representation",
     *      "etag": "W/\"4232\"",
     *      "location":"http://fhirtest.uhn.ca/baseDstu3/Observation/15354/_history/1"
     *      "correlationId": "xez5ZXQcDG6"
     *      "transactionId": "e6651fe2-fb8e-4a54-8b8e-7343dbdb997c"
     *      "security": ...
     *  },
     *  "body":
     *  {
     *       "resourceType": "Observation",
     *        ...
     *   }
     * }
     *
     * @param messageString String to parse.
     * @return Parsed message
     * @throws ParseException
     * @throws MessageParseException
     * @throws java.lang.ClassCastException
     */
    public static Message parse(String messageString) throws ParseException, MessageParseException, java.lang.ClassCastException, MessageIncompatibleException {
        Message message = new Message();

        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject)parser.parse(messageString);

        if (jsonObject.containsKey("header")) {
            JSONObject header = (JSONObject)jsonObject.get("header");

            if (header.containsKey("version")) {
                Version headerVersion = new Version((String)header.get("version"));
                if (!message.getVersion().isCompatible(headerVersion))
                    throw new MessageIncompatibleException("Version in message header incompatible with library "
                            + "version. Message header version = " + headerVersion
                            + ". Library version = " + message.getVersion());
                //Notice: We don't set the incoming headerVersion as the new headerVersion since we are parsing
                // everything into a Message that has version Message.getVersion().
            }
            else
                throw new MessageIncompatibleException("No headerVersion in header!");

            if (header.containsKey("sender"))
                message.setSender((String)header.get("sender"));
            else
                logger.warn("No sender in header!");

            if (header.containsKey("bodyCategory"))
                message.setBodyCategory(Message.bodyCategoryFromString((String)header.get("bodyCategory")));
            else
                logger.warn("No bodyCategory in header!");

            if (header.containsKey("bodyType"))
                message.setBodyType((String)header.get("bodyType"));
            else
                logger.warn("No bodyType in header!");

            if (header.containsKey("contentVersion"))
                message.setContentVersion((String)header.get("contentVersion"));
            else
                logger.warn("No contentVersion in header!");

            if (header.containsKey("prefer"))
                message.setPrefer(Message.preferFromString((String)header.get("prefer")));

            if (header.containsKey("etag"))
                message.setEtag((String)header.get("etag"));

            if (header.containsKey("ifMatch"))
                message.setIfMatch((String)header.get("ifMatch"));

            if (header.containsKey("ifNoneExist"))
                message.setIfNoneExist((String)header.get("ifNoneExist"));

            if (header.containsKey("location"))
                message.setLocation((String)header.get("location"));

            if (header.containsKey("correlationId"))
                message.setCorrelationId(MessagingUtils.verifyOrCreateId((String)header.get("correlationId")));
            else {
                logger.warn("No correlationId in header! One will be generated");
                message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
            }

            if (header.containsKey("transactionId"))
                message.setTransactionId((String)header.get("transactionId"));
            else
                logger.warn("No transcationId in header!");

            if (header.containsKey("security"))
                message.setSecurity((String)header.get("security"));

            if (header.containsKey("session"))
                message.setSession((String)header.get("session"));
        }
        else
            throw new MessageParseException("No header found in JSON");

        if (jsonObject.containsKey("body")) {
            message.setBody((String)jsonObject.get("body"));
        }
        else
            logger.warn("No body found in JSON - could be due to 'Prefer: return=minimal' header in requesting " +
                    "command");

        return message;
    }
}
