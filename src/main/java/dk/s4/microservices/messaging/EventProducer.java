package dk.s4.microservices.messaging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class EventProducer {
    private final static Logger logger = LoggerFactory.getLogger(EventProducer.class);

    private final List<IEventProducerInterceptor> interceptors = new ArrayList<>();

    public String sender;

    public EventProducer(String sender) {
        this.sender = sender;
    }

    /**
     * Call this method to send a message.
     *
     * Message sending may be intercepted by installed producer interceptors.
     *
     * @param topic to send message to.
     * @param message to send.
     */
    public void sendMessage(final Topic topic, Message message) {
        //Notify interceptors before sending message
        for (IEventProducerInterceptor next : interceptors) {
            boolean continueProcessing = next.interceptSend(topic, message);
            if (!continueProcessing) {
                logger.trace("Interceptor {} returned false, not continuing processing", next);
                return;
            }
        }
        doSendMessage(topic, message);
        for (IEventProducerInterceptor next : interceptors) {
            next.interceptSent(topic, message);
        }
    }

    /**
     * This method must implement the actual sending of the message to messaging system.
     *
     * @param topic to send this message to.
     * @param message to send.
     */
    protected abstract void doSendMessage(Topic topic, Message message);

    /**
     * Returns a list of all registered producer interceptors
     *
     * @return list of all registered producer interceptors
     */
    public List<IEventProducerInterceptor> getInterceptors() {
        return Collections.unmodifiableList(interceptors);
    }

    /**
     * Sets (or clears) the list of interceptors
     *
     * @param theInterceptors The list of interceptors (may be null)
     */
    public void setInterceptors(IEventProducerInterceptor... theInterceptors) {
        Validate.noNullElements(theInterceptors, "theInterceptors must not contain any null elements");

        interceptors.clear();
        if (theInterceptors != null) {
            interceptors.addAll(Arrays.asList(theInterceptors));
        }
    }

    /**
     * Sets (or clears) the list of interceptors
     *
     * @param theList The list of interceptors (may be null)
     */
    public void setInterceptors(List<IEventProducerInterceptor> theList) {
        interceptors.clear();
        if (theList != null) {
            interceptors.addAll(theList);
        }
    }
}
