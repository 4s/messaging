# Messaging library

[TOC]

Messaging is a library that supports sending and receiving messages over a Kafka based messaging system. The library
is a supporting library intented for use in a microservice based system as the one described at 
https://bitbucket.org/4s/gmk-gravide-med-komplikationer.

The library implements classes for message consumption, for message production and for serializing and deserializing
messages and topics.

The library has two different implementations of messaging: One that communicates directly with Apache Kafka and one
that mocks such communication.


## Topics
Topics can be serialized using the [Topic](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/Topic.java?at=master&fileviewer=file-view-default)
class. Topics can be deserialized using the [TopicParser](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/TopicParser.java?at=master&fileviewer=file-view-default)
class.

Topics are the unit of subscription in the publish/subscribe based messaging system. Topics are constructed
hierarchically according to the following scheme (EBNF notation):

```
Topic = Operation, "_", Category, "_", Type, { "_", Code }
```

Where the *Operation* can be either an event or a command: Events mean something *has* happened and commands
are *requests* for some action to take place.

*Category* specifies overall category of data or notification that is communicated on the this topic - e.g. "FHIR". 

*Type* is the specific type of data or notification communicated about on this topic - e.g. a HL7 FHIR "Observation". 
If it's a ProcessingFailed event you keep the Category, Type (and Code) of incoming message. 


*Code* is like a subtype specifiying particular flavor of data, typically in some official coding system - e.g. 
"MDC188736" specifying the
 code "188736" (MDC_MASS_BODY_ACTUAL) in the ISO MDC system. The *Code* part is only present if the data entity
 has such a code in a coding system at overall level (like e.g. HL7 FHIR Observation and DeviceComponent resources
 have). Several codes may be present in the Topic (typically in connection with a FHIR Bundle containing several 
 separate Observation resources).

**Example topics:**
```
InputReceived_FHIR_Observation_MDC188736
```

```
ProcessingFailed_FHIR_Observation_MDC188736
```
```
InputReceived_FHIR_Bundle_MDC188736_MDC150020_MDC149546
```

```
Error_System_Storage_ErrorCode127
```

```
Create_FHIR_Questionnaire
```


**Formal topic structure specified in [EBNF](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form):**
```
Topic = Operation, "_", Category, "_", Type, { "_", Code }

Operation = Event | Command

Event = "InputReceived" | "DataValidated" | "DataConverted" | "DataCreated" | "DataUpdated" | "TransactionCompleted" | "ProcessingOk" | "ProcessingFailed" | "Error"

Command = "Create" | "Update" | "Delete"

Category = "FHIR" | "CDA" | "System"

Type = FHIRResourceType | CDADocumentType | SystemErrorType

FHIRResourceType = "Bundle" | "Observation" | "Patient" | ... (see https://www.hl7.org/fhir/resourcelist.html)

CDADocumentType = "PHMR" | "QRD"

SystemErrorType = ...

Code= CodeSystem, CodeValue

CodeSystem= "MDC" (shorthand names for coding systems)

CodeValue=  alpha-numeric character, { alpha-numeric character }
```

## Messages
Messages are the content (or in Kafka terms records) produced to and consumed from topics. Messages can be serialized
 using the [Message](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/Message
 .java?at=master&fileviewer=file-view-default)
 class and can be deserialized using the [MessageParser](https://bitbucket
 .org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/MessageParser.java?at=master&fileviewer=file-view-default) class.

 Messages are formated in JSON and contain a *header* and body *part*. The *header* contains information about the
 payload data (the *body* part) and things like sender service, correlationId and security token(s).

**Header structure:**

  - sender: service name of sending service
  - bodyCategory: Overall category of data in body (FHIR | CDA | System)
  - contentVersion: Version of format of data in body (e.g. FHIR version)
  - prefer: Represents values for the "return" value as described at http://hl7.org/fhir/2018Sep/http.html#ops
  - etag: Etag header, in relation to FHIR see http://hl7.org/fhir/2018Sep/http.html#concurrency
  - ifMatch: If-Match header, in relation to FHIR see http://hl7.org/fhir/2018Sep/http.html#concurrency
  - ifNoneExist: If-None-Exist header, in relation to FHIR see https://www.hl7.org/fhir/http.html#ccreate
  - location: In response to CREATE or UPDATE commands will point to location of the resource created  
  (`<system>|<value>`)
  - correlationId: for logging and tracing messages
  - transactionId: UUID for the transaction that this message is part of
  - security: Security token
  - session: Session id. Typically used if authentication requires forwarding of session id with messages

**Example message:**

```
#!json
 {
   "header":
   {
       "sender": "obs-input-service",
       "bodyType": "FHIR",
       "contentVersion": "3.3.0",
       "correlationId": "xez5ZXQcDG6",
       "transactionId": "e6651fe2-fb8e-4a54-8b8e-7343dbdb997c",
       "security": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o"
   },
   "body":"{\"resourceType\":\"Bundle\",\"meta\":{\"lastUpdated\":\"2018-03-23T13:50:27+01:00\"}, ... "
 }
```

## Kafka implementation

### Producing messages
To produce messages to Kafka instantiate the class [KafkaEventProducer](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/kafka/KafkaEventProducer.java?at=master&fileviewer=file-view-default)
 and use KafkaEventProducer.sendMessage to send messages to Kafka on a particular topic:

```
#!java
...

Message message = new Message();
message.setBody(fhirResourceString);
message.setSender("My message sender");
message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
message.setTransactionId(UUID.randomUUID().toString());
message..setBodyCategory(Message.BodyCategory.FHIR);
message.setBodyType("Observation");
message.setContentVersion(System.getenv("FHIR_VERSION"));

KafkaEventProducer kafkaEventProducer = new KafkaEventProducer("My message sender");
kafkaEventProducer.sendMessage("my_topic", message);
```

### Consuming messages
The class [KafkaConsumeAndProcess](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/kafka/KafkaConsumeAndProcess.java?at=master&fileviewer=file-view-default)
handles consuming, processing and responding to messages.

To use this class you first need to define a class that implements the interface [EventProcessor](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/EventProcessor.java?at=master&fileviewer=file-view-default).
 The implementation of processMessage should contain the main processing of the body of the received message:

```
#!java
...
public class MyEventProcessor  implements EventProcessor {
    ...

    @Override
    public boolean processMessage(String consumedTopic, String receivedMessage,
                                     StringBuilder messageProcessedTopic,
                                     StringBuilder operationOutcome,
                                     StringBuilder location) {

        ... //process the body of the received message
    }
}
```
Second, you need an instance of the [KafkaEventProducer](https://bitbucket.org/4s/messaging/src/master/src/main/java/dk/s4/microservices/messaging/kafka/KafkaEventProducer.java?at=master&fileviewer=file-view-default)
class. This instance is for use by *KafkaConsumeAndProcess* for producing result messages.

Example instantiation of *KafkaConsumeAndProcess*:

```
#!java
...
		try {
			KafkaEventProducer kafkaEventProducer = new KafkaEventProducer("FHIRObs2PHMR");
			EventProcessor eventProcessor = new MyEventProcessor();
			new Thread((new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor))).start();
		} catch (KafkaInitializationException | MessagingInitializationException e) {
			logger.error("Error during Kafka initialization: "+ e.getMessage(), e);
			System.exit(0);
		}

```
