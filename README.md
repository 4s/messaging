# Messaging library

| Summary                          |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Library name (computer friendly) | messaging                                                                                                        |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                         |
| License                          | Apache 2.0                                                                                                          |

Messaging is a library that supports sending and receiving messages over a Kafka based messaging system. The library
is a supporting library intented for use in a microservice based system as the one described at 
https://bitbucket.org/4s/gmk-gravide-med-komplikationer.

## Documentation
  * Design and messaging protocol: [docs/Main.md](docs//Main.md)
  * Public classes and methods are documented with JavaDoc 

## Prerequisites
To build the project you need:

  * OpenJDK 11+
  * Maven 3.1.1+

To test the project you need 
  
  * Access to a running instance of Apache Kafka 2.3.0+

## Building:
```
mvn clean package
```

## Environment variables
The file `kafka.env` contains the environment variables that you need to use if you include and use this
library in you own services.

**kafka.env environment variables:**
`KAFKA_BOOTSTRAP_SERVER` should be set to the host and port of your Kafka server. I.e.  the host/port pair to use for 
establishing the initial connection to the Kafka cluster. This corresponds to the native Kafka producer and consumer 
config called *bootstrap.servers* - see https://kafka.apache.org/documentation/#configuration - except that the messaging 
library currently only supports one server, not a list of servers like native Kafka does.

All other environment variables starting with ``KAFKA_`` map directly to a corresponding [Kafka configuration key](https://kafka.apache.org/documentation/#configuration) 
following a scheme where you strip off  the ``KAFKA_`` part and replace `_` with a `.`. For instance `KAFKA_KEY_DESERIALIZER`
corresponds to the native Kafka consumer property `key.deserializer`

You should not change the values of `KAFKA_KEY_DESERIALIZER`, `KAFKA_VALUE_DESERIALIZER`, `KAFKA_KEY_SERIALIZER`, 
KAFKA_VALUE_SERIALIZER` and `KAFKA_ENABLE_AUTO_COMMIT`.`

Currently only the following Kafka related environment variables are supported:
```
KAFKA_BOOTSTRAP_SERVER

#KafkaConsumer
KAFKA_KEY_DESERIALIZER
KAFKA_VALUE_DESERIALIZER
KAFKA_ENABLE_AUTO_COMMIT
KAFKA_AUTO_COMMIT_INTERVAL_MS
KAFKA_SESSION_TIMEOUT_MS
KAFKA_GROUP_ID

#KafkaProducer
KAFKA_ACKS
KAFKA_RETRIES
KAFKA_KEY_SERIALIZER
KAFKA_VALUE_SERIALIZER
KAFKA_MAX_REQUEST_SIZE
```
### Consumer health
With the following environment variables you may enable that instances of the KafkaProcessAndConsume class touch a 
file at specified intervals:
``` 
ENABLE_HEALTH=true
HEALTH_FILE_PATH=/tmp/health
HEALTH_INTERVAL_MS=5000
```
This can be used to check the health of the KafkaProcessAndConsume thread - typically by having a shell script check 
on the timestamp of the health file.

## Test
__To run unit tests:__
```
mvn clean test
```

__To run integration tests:__

For the integration tests to succeed you need access to a running instance of Apache Kafka and 
KAFKA_BOOTSTRAP_SERVER in kafka.env and bootstrap.servers in KafkaProducer.props (in test/resources) must point to 
the Kafka broker of this instance. 

Furthermore you need to ensure the instance of Kafka you're testing against doesn't already have data on the topics
you are testing against. To ensure this delete these topics (assumes your kafka is running inside a docker container):

```
docker exec <docker-container-name> /opt/kafka/bin/kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic InputReceived_FHIR_Observation_MDC29112
docker exec <docker-container-name> /opt/kafka/bin/kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic DataCreated_FHIR_Observation_MDC29112
```

Now run:
```
mvn clean verify -Pfailsafe
```